//
//  WhistleWarnings.h
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 12.03.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

#ifndef WhistleWarnings_h
#define WhistleWarnings_h

#define WHISTLE_DEPRECATION_ON											\
_Pragma("clang diagnostic push")										\
_Pragma("clang diagnostic ignored \"-Wdeprecated-declarations\"")		\
_Pragma("clang diagnostic ignored \"-Wdeprecated-implementations\"")

#define WHISTLE_DEPRECATION(expr)										\
do {																	\
_Pragma("clang diagnostic push")										\
_Pragma("clang diagnostic ignored \"-Wdeprecated-declarations\"")		\
_Pragma("clang diagnostic ignored \"-Wdeprecated-implementations\"")	\
expr;																	\
_Pragma("clang diagnostic pop")											\
} while(0)

#define WHISTLE_PERFORM_SELECTOR_LEAKS_ON								\
_Pragma("clang diagnostic push")										\
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")

#define WHISTLE_PERFORM_SELECTOR_LEAKS(expr)							\
do {																	\
_Pragma("clang diagnostic push")										\
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")		\
expr;																	\
_Pragma("clang diagnostic pop")											\
} while(0)

#define WHISTLE_WARNINGS_OFF											\
_Pragma("clang diagnostic pop")

#endif /* WHISTLEWarnings_h */
