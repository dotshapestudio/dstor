//
//  TabSecurity.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 03.05.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

@objcMembers
class TabProtection: NSObject {

	enum stage: String, CustomStringConvertible {

		case alwaysRemember = "always_remember"
		case forgetOnShutdown = "forget_on_shutdown"
		case clearOnBackground = "clear_on_background"

		var description: String {
			switch self {
			case .alwaysRemember:
				return NSLocalizedString("Continue where you left off", comment: "Tab security level")

			case .forgetOnShutdown:
				return NSLocalizedString("Open the New Tab page", comment: "Tab security level")

			default:
				return NSLocalizedString("Forget in Background", comment: "Tab security level")
			}
		}
	}

//	class var isClearOnBackground: Bool {
//		return DSSettings.tabSecurity  == .clearOnBackground
//	}

	/**
	Handle tab privacy
	*/
	class func background_handling() {
		let security = Configrations.tabProtection
		let controller = AppDelegate.shared?.browsingUi
		let cookieJar = AppDelegate.shared?.cookieJar
		let ocspCache = AppDelegate.shared?.certificateAuthentication

		if security == .clearOnBackground {
			controller?.deleteAllTabs()
		}
		else {
			cookieJar?.clearAllOldNonWhitelistedData()
			ocspCache?.persist()
		}

		if security == .alwaysRemember {
			// Ignore special URLs, as these could get us into trouble after app updates.
			Configrations.showTabs = controller?.tabList.map({ $0.link }).filter({ !$0.isUnique })

			print("[\(String(describing: self))] save open tabs=\(String(describing: Configrations.showTabs))")
		}
		else {
			print("[\(String(describing: self))] clear saved open tab urls")
            Configrations.showTabs = nil
		}
	}

	class func reset() {
		if Configrations.tabProtection  == .alwaysRemember,
			let controller = AppDelegate.shared?.browsingUi {

			for url in Configrations.showTabs ?? [] {
				// Ignore special URLs, as these could get us into trouble after app updates.
				if !url.isUnique {
					print("[\(String(describing: self))] restore tab with url=\(url)")

					controller.insertTab(url, transition: .notAnimated)
				}
			}
		}
		else {
			print("[\(String(describing: self))] clear saved open tab urls")
            Configrations.showTabs = nil
		}
	}
}
