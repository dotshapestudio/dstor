//
//  Ipv6Manger.h
//  OnionBrowser2
//
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

#define TOR_IPV6_CONNECTION_FALSE 0
#define TOR_IPV6_CONNECTION_DUAL 1
#define TOR_IPV6_CONNECTION_ONLY 2
#define TOR_IPV6_CONNECTION_UNKNOWN 99

@interface Ipv6Manger : NSObject

+ (NSInteger) status;

@end
