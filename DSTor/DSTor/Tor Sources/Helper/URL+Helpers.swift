//
//  URL+Helpers.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 21.11.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import Foundation

extension URL {

	static let empty = URL(string: "about:blank")!

	static let aboutDSTor = URL(string: "about:onion-browser")!
	static let resources_details = Bundle.main.url(forResource: "credits", withExtension: "html")!

	static let securityLevels_page = URL(string: "about:security-levels")!
	static let securityLevels = Bundle.main.url(forResource: "security-levels", withExtension: "html")!

    static var start: URL {
        return URL(string: Configrations.searchEngine?.homeUrl ?? "https://duckduckgo.com")!
    }

	var alongFixedScheme: URL? {
		switch scheme?.lowercased() {
		case "onionhttp":
			var urlc = URLComponents(url: self, resolvingAgainstBaseURL: true)
			urlc?.scheme = "http"

			return urlc?.url

		case "onionhttps":
			var urlc = URLComponents(url: self, resolvingAgainstBaseURL: true)
			urlc?.scheme = "https"

			return urlc?.url

		default:
			return self
		}
	}

	var actual: URL {
		switch self {
		case URL.aboutDSTor:
			return URL.resources_details

		case URL.securityLevels_page:
			return URL.securityLevels

		default:
			return self
		}
	}

	var beautify: URL? {
		switch self {
		case URL.resources_details:
			return URL.aboutDSTor

		case URL.securityLevels:
			return URL.securityLevels_page

		case URL.start:
            return URL.start

		default:
			return self
		}
	}

	var isUnique: Bool {
		switch self {
		case URL.empty, URL.aboutDSTor, URL.resources_details, URL.securityLevels_page, URL.securityLevels:
			return true

		default:
			return false
		}
	}
}

@objc
extension NSURL {

	var alongFixedScheme: NSURL? {
		return (self as URL).alongFixedScheme as NSURL?
	}
}
