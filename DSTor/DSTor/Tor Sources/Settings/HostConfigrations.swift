//
//  HostConfigrations.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 16.12.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

extension NSNotification.Name {
	static let hostSettingsReplaced = NSNotification.Name(rawValue: HostConfigrations.hostConfigsReplaced)
}

class HostConfigrations: NSObject {

	@objc
	static let hostConfigsReplaced = "host_settings_changed"

	@objc
	enum TorContentPolicy: Int, CustomStringConvertible,CaseIterable {
        case open
        case blockXhr
        case strict

		var key: String {
			switch self {
			case .open:
				return "open"

			case .blockXhr:
				return "block_connect"

			default:
				return "strict"
			}
		}

		var description: String {
			switch self {
			case .open:
				return NSLocalizedString("Open (normal browsing mode)",
				comment: "Content policy option")

			case .blockXhr:
				return NSLocalizedString("No XHR/WebSocket/Video connections",
				comment: "Content policy option")

			default:
				return NSLocalizedString("Strict (no JavaScript, video, etc.)",
				comment: "Content policy option")
			}
		}

		init(_ value: String?) {
			switch value {
			case TorContentPolicy.open.key:
				self = .open

			case TorContentPolicy.blockXhr.key:
				self = .blockXhr

			default:
				self = .strict
			}
		}
    }

	private static let file_link: URL? = {
		return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
			.first?.appendingPathComponent("host_settings.plist")
	}()

	// Keys are left as-is to maintain backwards compatibility.

	private static let host_default = "__default"
	private static let `true` = "1"
	private static let `false` = "0"
	private static let ignoreErrorKeysOfTLS = "ignore_tls_errors"
	private static let allowedCookiesKey = "whitelist_cookies"
	private static let key_webRTC_protocol = "allow_webrtc"
	private static let key_mixedMode_Resouces = "allow_mixed_mode"
	private static let keyForULP = "universal_link_protection"
	private static let keyToFollowTorLocationHeader = "follow_onion_location_header"
	private static let key_userAgent = "user_agent"
	private static let key_contentPolicy = "content_policy"

	private static var _fresh: [String: [String: String]]?
	private static var fresh: [String: [String: String]] {
		get {
			if _fresh == nil, let url = file_link {
				_fresh = NSDictionary(contentsOf: url) as? [String: [String: String]]

				// Fix later introduced setting, which defaults to true.
				_fresh?[HostConfigrations.host_default]?[keyToFollowTorLocationHeader] = HostConfigrations.true
			}

			return _fresh ?? [:]
		}
		set {
			_fresh = newValue
		}
	}

	static var hosts: [String] {
		return fresh.keys.filter({ $0 != HostConfigrations.host_default }).sorted()
	}

	/**
	- returns: The default settings. If nothing stored, yet, will return a full set of (unpersisted) defaults.
	*/
	class func byDefault() -> HostConfigrations {
		return `for`(nil)
	}

	/**
	- If the host is nil or empty, will return default settings.
	- If there are stored settings for the host, will return these.
	- If there are no settings stored for the host, will return a new (unpersisted) object with empty settings,
		which will automatically walk up the domain levels in search of a setting and finally end at the default
		settings.

	- parameter host: The host name. Can be `nil` which will return the default settings.
	- returns: Settings for the given host.
	*/
	@objc
	class func `for`(_ host: String?) -> HostConfigrations {
		// If no host given, return default host settings.
		guard let host = host, !host.isEmpty else {
			// If user-customized default host settings available, return these.
			if has(host_default) {
				return HostConfigrations(for: host_default, raw: fresh[host_default]!)
			}

			// ...else return hardcoded defaults.
			return HostConfigrations(for: host_default, withDefaults: true)
		}

		// If user-customized settings for this host available, return these.
		if has(host) {
			return HostConfigrations(for: host, raw: fresh[host]!)
		}

		// ...else return new empty settings for this host which will trigger
		// fall through logic to higher domain levels or default host settings.
		return HostConfigrations(for: host, withDefaults: false)
	}

	/**
	Check, if we have explicit settings for a given host.

	This will *not* check the domain level hirarchy!

	- parameter host: The host name.
	- returns: true, if we have explicit settings for that host, false, if not.
	*/
	class func has(_ host: String?) -> Bool {
		return !(host?.isEmpty ?? true) && fresh.keys.contains(host!)
	}

	/**
	Remove settings for a specific host.

	You are not allowed to remove the default host settings!

	- parameter host: The host name.
	- returns: Class for fluency.
	*/
	@discardableResult
	class func terminate(_ host: String) -> HostConfigrations.Type {
		if host != host_default && has(host) {
			fresh.removeValue(forKey: host)

			DispatchQueue.main.async {
				NotificationCenter.default.post(name: .hostSettingsReplaced, object: host)
			}
		}

		return self
	}

	/**
	Persists all settings to disk.
	*/
	@objc
	class func add() {
		if let url = file_link {
			(fresh as NSDictionary).write(to: url, atomically: true)
		}
	}

	private var fresh: [String: String]

	/**
	The host name these settings apply to.
	*/
	let host: String

	/**
	True, if TLS errors should be ignored. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var tlsErrorIgnored: Bool {
		get {
			return take(HostConfigrations.ignoreErrorKeysOfTLS) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.ignoreErrorKeysOfTLS] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	/**
	True, if cookies for this host should be whitelisted. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var allowedCookiesKey: Bool {
		get {
			return take(HostConfigrations.allowedCookiesKey) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.allowedCookiesKey] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	/**
	True, if WebRTC should be allowed. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var option_webRTC: Bool {
		get {
			return take(HostConfigrations.key_webRTC_protocol) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.key_webRTC_protocol] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	/**
	True, if mixed-mode resources (mixing HTTPS and HTTP) should be allowed. Will walk up the domain levels
	ending at the default settings, if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var option_mixedModeResources: Bool {
		get {
			return take(HostConfigrations.key_mixedMode_Resouces) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.key_mixedMode_Resouces] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	/**
	True, if universal link protection should be applied. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	var option_ULP: Bool {
		get {
			return take(HostConfigrations.keyForULP) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.keyForULP] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	@objc
	var option_followTorLocationHeader: Bool {
		get {
			take(HostConfigrations.keyToFollowTorLocationHeader) == HostConfigrations.true
		}
		set {
			fresh[HostConfigrations.keyToFollowTorLocationHeader] = newValue
				? HostConfigrations.true
				: HostConfigrations.false
		}
	}

	/**
	User Agent string to use. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var option_userAgent: String {
		get {
			return take(HostConfigrations.key_userAgent)
		}
		set {
			fresh[HostConfigrations.key_userAgent] = newValue
		}
	}

	/**
	Content policy to apply. Will walk up the domain levels ending at the default settings,
	if not explicitly set for this host.

	Setting this will always set the value explicitly for this host.
	*/
	@objc
	var option_contentPolicy: TorContentPolicy {
		get {
			return TorContentPolicy(take(HostConfigrations.key_contentPolicy))
		}
		set {
			fresh[HostConfigrations.key_contentPolicy] = newValue.key
		}
	}

	/**
	Will be used by `HostSettings.for()`.

	- parameter host: The host name.
	- parameter raw: The raw stored data as a dictionary.
	*/
	private init(for host: String, raw: [String: String]) {
		self.host = host
		self.fresh = raw
	}

	/**
	Create a new HostSettings. It is not added to the persistent configuration until you call #save!

	If you create default host settings, all settings will always be created hard, regardless of the `withDefaults`
	flag.

	- parameter host: The host name.
	- parameter withDefaults: When true, all settings from the default host will be copied, when false,
		settings will not be set at all and you will be able to override specific things while the (ever changing)
		default host settings will still apply for the rest.
	*/
	@objc
	init(for host: String, withDefaults: Bool) {
		self.host = host

		if host == HostConfigrations.host_default {
			fresh = [
				HostConfigrations.ignoreErrorKeysOfTLS: HostConfigrations.false,
				HostConfigrations.allowedCookiesKey: HostConfigrations.false,
				HostConfigrations.key_webRTC_protocol: HostConfigrations.false,
				HostConfigrations.key_mixedMode_Resouces: HostConfigrations.false,
				HostConfigrations.keyForULP: HostConfigrations.true,
				HostConfigrations.keyToFollowTorLocationHeader: HostConfigrations.true,
				HostConfigrations.key_userAgent: "",
				HostConfigrations.key_contentPolicy: HostConfigrations.TorContentPolicy.strict.key,
			]
		}
		else {
			if withDefaults {
				fresh = HostConfigrations.byDefault().fresh
			}
			else {
				fresh = [:]
			}
		}
	}

	/**
	Add this host's settings to the persistent data store and post  the
	`NSNotification.Name.hostSettingsReplaced`.

	Call #store afterwards to persist all settings to disk!

	- returns: The object's type so you can chain #store to it, if you want.
	*/
	@discardableResult
	@objc
	func saveCopy() -> HostConfigrations.Type {
		HostConfigrations.fresh[host] = fresh

		let host = self.host == HostConfigrations.host_default ? nil : self.host

		DispatchQueue.main.async {
			NotificationCenter.default.post(name: .hostSettingsReplaced, object: host)
		}

		return type(of: self)
	}

	/**
	Walk up the chain of hosts from `z.y.x.example.com` up to `example.com` to find a dedicated setting.
	If none found, return the setting of the default host.

	- parameter key: The setting's key.
	- returns: A string containing the searched setting.
	*/
	private func take(_ key: String) -> String {
		if let value = fresh[key] {
			return value
		}

		// Stop endless recursion. This might happen if you search for
		// a setting which is not defined.
		if host == HostConfigrations.host_default {
			return ""
		}

		// Don't ommit empty subsequences to cater to hosts which containing
		// a leading period, like ".example.com". This mostly happens with cookie handling.
		var parts = host.split(separator: ".", omittingEmptySubsequences: false)

		while parts.count > 1 {
			parts.removeFirst()

			let superhost = parts.joined(separator: ".")

			if HostConfigrations.has(superhost) {
				return HostConfigrations.for(superhost).take(key)
			}
		}

		return HostConfigrations.byDefault().take(key)
	}
}
