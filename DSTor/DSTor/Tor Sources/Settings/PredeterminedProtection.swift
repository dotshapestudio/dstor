//
//  SecurityPresetsRow.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 16.10.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

enum PredeterminedProtection: Int, CustomStringConvertible {
	var description: String {
		switch self {
		case .insecure:
			return NSLocalizedString("Bronze", comment: "Security level")

		case .medium:
			return NSLocalizedString("Silver", comment: "Security level")

		case .secure:
			return NSLocalizedString("Gold", comment: "Security level")

		default:
			return NSLocalizedString("Custom", comment: "Security level")
		}
	}

	var suggestion: String? {
		switch self {
		case .insecure:
			return NSLocalizedString("(not recommended)", comment: "")

		default:
			return nil
		}
	}

	var clarification: String {
		switch self {
		case .insecure:
			return NSLocalizedString("Easy breezy. Though it could be dangerous.", comment: "")

		case .medium:
			return NSLocalizedString("Works pretty well. Your identity is mostly protected.", comment: "")

		case .secure:
			return NSLocalizedString("Websites may break. But you get great security.", comment: "")

		default:
			return NSLocalizedString("Settings have been customized.", comment: "")
		}
	}

	var data: (csp: HostConfigrations.TorContentPolicy, webRtc: Bool, mixedMode: Bool)? {
		switch self {
		case .insecure:
			return (.open, true, true)

		case .medium:
			return (.blockXhr, false, false)

		case .secure:
			return (.strict, false, false)

		default:
			return nil
		}
	}

	case custom = -1
	case insecure = 0
	case medium = 1
	case secure = 2

	init(_ csp: HostConfigrations.TorContentPolicy?, _ webRtc: Bool?, _ mixedMode: Bool?) {
		let webRtc = webRtc ?? false
		let mixedMode = mixedMode ?? false

		if csp == .open && webRtc && mixedMode {
			self = .insecure
		}
		else if csp == .blockXhr && !webRtc && !mixedMode {
			self = .medium
		}
		else if csp == .strict && !webRtc && !mixedMode {
			self = .secure
		}
		else {
			self = .custom
		}
	}

	init(_ settings: HostConfigrations?) {
		self = .init(settings?.option_contentPolicy,
					 settings?.option_webRTC,
					 settings?.option_mixedModeResources)
	}
}
