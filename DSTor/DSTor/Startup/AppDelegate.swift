//
//  AppDelegate.swift
//  DSTor
//
//  Created by Shahbaz Akram on 15/04/2021.
//

import UIKit
import LocalAuthentication

@main
class AppDelegate: UIResponder, UIApplicationDelegate,JAHPAuthenticatingHTTPProtocolDelegate {
    
    private var alert: UIAlertController?
    var window:UIWindow?
    
    /**
    Flag, if biometric/password authentication after activation was successful.

    Return to false immediately after positive check, otherwise, security issues will arise!
    */
    private var verified = false
    
    private var inStartupPhase = true
    
    @objc
    static let socksProxyPort = 39050

    @objc
    static let httpProxyPort = 0

    @objc
    class var shared: AppDelegate? {
        var delegate: UIApplicationDelegate?

        if Thread.isMainThread {
            delegate = UIApplication.shared.delegate
        }
        else {
            DispatchQueue.main.sync {
                delegate = UIApplication.shared.delegate
            }
        }

        return delegate as? AppDelegate
    }
    
    /**
     Some sites do mobile detection by looking for Safari in the UA, so make us look like Mobile Safari

     from "Mozilla/5.0 (iPhone; CPU iPhone OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Mobile/12H321"
     to   "Mozilla/5.0 (iPhone; CPU iPhone OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H321 Safari/600.1.4"
     */
    let defaultUserAgent: String? = {
        var uaparts = UIWebView(frame: .zero)
            .stringByEvaluatingJavaScript(from: "navigator.userAgent")?
            .components(separatedBy: " ")

        // Assume Safari major version will match iOS major.
        let osv = UIDevice.current.systemVersion.components(separatedBy: ".")
        let index = (uaparts?.endIndex ?? 1) - 1
        uaparts?.insert("Version/\(osv.first ?? "0").0", at: index)

        // Now tack on "Safari/XXX.X.X" from WebKit version.
        for p in uaparts ?? [] {
            if p.contains("AppleWebKit/") {
                uaparts?.append(p.replacingOccurrences(of: "AppleWebKit", with: "Safari"))
                break
            }
        }

        return uaparts?.joined(separator: " ")
    }()

    @objc
    let sslCertCache = NSCache<NSString, SSLCertificate>()

    @objc
    let certificateAuthentication = CertificateAuthentication()

    @objc
    let hstsCache = HSTSCache.retrieve()

    @objc
    let cookieJar = CookieJar()

    @objc
    var browsingUi: DSMainWebController?
    
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        JAHPAuthenticatingHTTPProtocol.setDelegate(self)
        JAHPAuthenticatingHTTPProtocol.start()
        
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // MARK: Main Controller embedded in navigationController added as Root ViewController
        
        let nav: UINavigationController = UINavigationController(rootViewController: DSSplashVC())
        nav.navigationBar.isHidden = true
        show(nav)
        
        return true
    }
    
    func show(_ viewController: UIViewController?, _ completion: ((Bool) -> Void)? = nil) {
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.backgroundColor = .white
        }

        if viewController?.restorationIdentifier == nil {
            viewController?.restorationIdentifier = String(describing: type(of: viewController))
        }
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()

        UIView.transition(with: window!, duration: 0.3, options: .transitionCrossDissolve,
                          animations: {}, completion: completion)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        application.ignoreSnapshotOnNextApplicationLaunch()
        browsingUi?.resignFRToSearchField()

        BlurredScreenShot.make()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        HostConfigrations.add()
        DispatchQueue.global(qos: .background).async {
            let taskId = application.beginBackgroundTask(expirationHandler: nil)

            self.hstsCache?.persist()

            application.endBackgroundTask(taskId)
        }
        TabProtection.background_handling()

        application.ignoreSnapshotOnNextApplicationLaunch()

        if DSTorManager.shared.state != .stopped {
            DSTorManager.shared.stopTorBrowser()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        cookieJar.clearAllNonWhitelistedData()
        DownloadHelper.deleteDownloadsDirectory()
        DSOnExitSettingsViewModel.eraseSettingsOnExit()

//        DispatchQueue.global(qos: .background).async {
//            let taskId = application.beginBackgroundTask(expirationHandler: nil)
//            DSOnExitSettingsViewModel.eraseSettingsOnExit()
////            application.endBackgroundTask(taskId)
//        }
//        
        application.ignoreSnapshotOnNextApplicationLaunch()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if shouldDeleteLockKey() {
            _ = ProtectedEnclave.delete_key()
        }

        if !verified, let privateKey = ProtectedEnclave.add_key() {
            var counter = 0

            repeat {
                let nonce = ProtectedEnclave.getNow()

                verified = ProtectedEnclave.authenticate(
                    nonce, signature: ProtectedEnclave.sign(nonce, with: privateKey),
                    with: ProtectedEnclave.getPublic_key(privateKey))

                counter += 1
            } while !verified && counter < 3

            if !verified {
                applicationWillResignActive(application)
                applicationDidEnterBackground(application)
                applicationWillTerminate(application)

                exit(0)
            }

            // Always return here, as the SecureEnclave operations will always
            // trigger a user identification and therefore the app becomes inactive
            // and then active again. So #applicationDidBecomeActive will be
            // called again. Therefore, we store the result of the verification
            // in an object property and check that on re-entry.
            return
        }

        verified = false

        if window?.rootViewController == nil {
            show(DSSplashVC())
        }
        else {
            BlurredScreenShot.delete()
        }

        let mgr = DSTorManager.shared

        if (!inStartupPhase && mgr.state != .started && mgr.state != .connected) {
            // Difficult situation to add a delegate here, so we never did.
            // Turns out, it is no problem. Tor seems to always restart correctly.
            
            mgr.startTorBrowser(delegate: nil)
            
//            let splashVC = window?.rootViewController as! UINavigationController
//            let splash = splashVC.viewControllers.last as! DSSplashVC
//            mgr.startTorBrowser(delegate: splash.viewModel)
        }
        else {
            inStartupPhase = false
        }
    }
    
    // MARK: JAHPAuthenticatingHTTPProtocolDelegate

    func authenticatingHTTPProtocol(_ authenticatingHTTPProtocol: JAHPAuthenticatingHTTPProtocol?,
                                    logMessage message: String) {
        print("[JAHPAuthenticatingHTTPProtocol] \(message)")
    }

    func authenticatingHTTPProtocol(_ authenticatingHTTPProtocol: JAHPAuthenticatingHTTPProtocol,
                                    canAuthenticateAgainstProtectionSpace protectionSpace: URLProtectionSpace)
        -> Bool {

        return protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPDigest
            || protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic
    }

    func authenticatingHTTPProtocol(_ authenticatingHTTPProtocol: JAHPAuthenticatingHTTPProtocol,
                                    didReceive challenge: URLAuthenticationChallenge)
        -> JAHPDidCancelAuthenticationChallengeHandler? {

        let space = challenge.protectionSpace
        let storage = URLCredentialStorage.shared

        // If we have existing credentials for this realm, try them first.
        if challenge.previousFailureCount < 1,
            let credential = storage.credentials(for: space)?.first?.value {

            storage.set(credential, for: space)
            authenticatingHTTPProtocol.resolvePendingAuthenticationChallenge(with: credential)

            return nil
        }

        DispatchQueue.main.async {
            self.alert = AlertManager.generate(
                message: (space.realm?.isEmpty ?? true) ? space.host : "\(space.host): \"\(space.realm!)\"",
                title: NSLocalizedString("Authentication Required", comment: ""))

            AlertManager.addNameField(self.alert!, placeholder:
                NSLocalizedString("Username", comment: ""))

            AlertManager.addPassField(self.alert!, placeholder:
                NSLocalizedString("Password", comment: ""))

            self.alert?.addAction(AlertManager.cancelButton { _ in
                challenge.sender?.cancel(challenge)
                authenticatingHTTPProtocol.client?.urlProtocol(
                    authenticatingHTTPProtocol,
                    didFailWithError: NSError(domain: NSCocoaErrorDomain,
                                              code: NSUserCancelledError,
                                              userInfo: [ORIGIN_KEY: true]))
            })

            self.alert?.addAction(AlertManager.defaultButton(NSLocalizedString("Log In", comment: "")) { _ in
                // We only want one set of credentials per protectionSpace.
                // In case we stored incorrect credentials on the previous
                // login attempt, purge stored credentials for the
                // protectionSpace before storing new ones.
                for c in storage.credentials(for: space) ?? [:] {
                    storage.remove(c.value, for: space)
                }

                let textFields = self.alert?.textFields

                let credential = URLCredential(user: textFields?.first?.text ?? "",
                                               password: textFields?.last?.text ?? "",
                                               persistence: .forSession)

                storage.set(credential, for: space)
                authenticatingHTTPProtocol.resolvePendingAuthenticationChallenge(with: credential)
            })

            self.window?.rootViewController?.top.present(self.alert!)
        }

        return nil
    }

    func authenticatingHTTPProtocol(_ authenticatingHTTPProtocol: JAHPAuthenticatingHTTPProtocol,
                                    didCancel challenge: URLAuthenticationChallenge) {

        if (alert?.isViewLoaded ?? false) && alert?.view.window != nil {
            alert?.dismiss(animated: false)
        }
    }
    
    private func shouldDeleteLockKey() -> Bool {
        let devicelockEnabled: Bool = LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        let prevValue: Bool = ProtectedEnclave.add_key() != nil
    
        return !devicelockEnabled && prevValue
    }
}

