//
//  DSCreateNavigationView.swift
//  DSTor
//
//  Created by Macbook on 29/06/2021.
//

import UIKit

// MARK: Class to Create the TopNavigationView and manage events accordingly

class DSNavigationView: UIView {
    
    private let theme_color: UIColor = .getColor(.DSPurple)
    private var presentedController: UIViewController!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    init(title: String, controller: UIViewController) {
        super.init(frame: .zero)
        
        presentedController = controller
        presentedController.view.addSubview(self)
        setupView(title: title)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(title: String)
    {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = theme_color
        
        centerLabel(title: title)
        closeButton()
        
        NSLayoutConstraint.activate([
            self.rightAnchor.constraint(equalTo: presentedController.view.rightAnchor),
            self.leftAnchor.constraint(equalTo: presentedController.view.leftAnchor),
            self.topAnchor.constraint(equalTo: presentedController.view.topAnchor),
            self.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func centerLabel(title: String)
    {
        let lblTitle: UILabel = UILabel()
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.textAlignment = .center
        lblTitle.textColor = .white
        lblTitle.font = UIFont.systemFont(ofSize: 18)
        lblTitle.text = title
        
        self.addSubview(lblTitle)
        
        NSLayoutConstraint.activate([
            lblTitle.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            lblTitle.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            lblTitle.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65)
        ])
    }
    
    private func closeButton()
    {
        let closeButton: UIButton = UIButton()
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.setTitle("", for: .normal)
        closeButton.setImage(UIImage(imageLiteralResourceName: "close"), for: .normal)
        closeButton.contentVerticalAlignment = .center
        closeButton.contentHorizontalAlignment = .center
        closeButton.backgroundColor = .white
        closeButton.layer.cornerRadius = 12.5
        closeButton.layer.masksToBounds = true
        closeButton.addTarget(self, action: #selector(closeButtonClicked), for: .touchUpInside)
        self.addSubview(closeButton)
        
        // MARK: btnDismiss Constarints
        NSLayoutConstraint.activate([
            closeButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15),
            closeButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            closeButton.widthAnchor.constraint(equalToConstant: 25),
            closeButton.heightAnchor.constraint(equalToConstant: 25)
        ])
    }
    
    @IBAction private func closeButtonClicked(_ sender: Any)
    {
        presentedController.dismiss(animated: true, completion: nil)
    }
    
}
