//
//  DSBridgesVC.swift
//  DSTor
//
//  Created by Macbook on 13/07/2021.
//

import UIKit

class DSBridgesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let tableView: UITableView = UITableView()
    private let theme_color: UIColor = .getColor(.DSPurple)
    private let viewModel: DSBridgesVM = DSBridgesVM()
    var delegate: DSSettingListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        weak var weakSelf: DSBridgesVC? = self
        DispatchQueue.main.async {
            weakSelf?.generateUI()
        }
    }
    
    // MARK: UI Private Functions

    private func addTableView(topView: UIView) {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.view.addSubview(tableView)
        
        // MARK: TableView Constarints
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: topView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func getTableViewCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell()
        
        cell.textLabel!.font = .systemFont(ofSize: 16)
        cell.textLabel!.textColor = theme_color
        cell.accessoryType = .none
        cell.tintColor = theme_color
        let dataToPopulate: DSBridgesVM.BridgeMap = DSBridgesVM.section1Data[indexPath.row]
        cell.textLabel!.text = dataToPopulate.strType
        cell.textLabel!.textAlignment = .left
        cell.accessoryType = (dataToPopulate.type == viewModel.currentBridgesType) ? .checkmark : .none
        
        let spInsets: UIEdgeInsets = cell.separatorInset
        cell.separatorInset = UIEdgeInsets(top: spInsets.top, left: 0, bottom: spInsets.bottom, right: spInsets.right)
        cell.backgroundColor = .white
        
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: UI Generation
    
    private func generateUI() {
        self.view.backgroundColor = .white
        let top: UIView = DSNavigationView(title: "Bridges Configration", controller: self)
        addTableView(topView: top)
    }
    
    // MARK: UITableView Delegates and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DSBridgesVM.section1Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return getTableViewCell(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowData: DSBridgesVM.BridgeMap = DSBridgesVM.section1Data[indexPath.row]
        if rowData.type != .custom {
            viewModel.currentBridgesType = rowData.type
            viewModel.connect()
            delegate?.needsReload()
            tableView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }
        } else {
            let customBridgesVC: DSCustomBridgesVC = DSCustomBridgesVC()
            customBridgesVC.customBridgesSelected = {
                DispatchQueue.main.async { [weak self] in
                    self?.delegate?.needsReload()
                    self?.tableView.reloadData()
                }
            }
            customBridgesVC.viewModel = viewModel
            customBridgesVC.modalPresentationStyle = .formSheet
            self.present(customBridgesVC, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
