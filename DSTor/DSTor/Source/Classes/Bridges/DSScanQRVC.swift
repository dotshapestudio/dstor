//
//  DSScanQRVC.swift
//  DSTor
//
//  Created by Macbook on 15/07/2021.
//

import Foundation
import AVFoundation

class DSScanQRVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var viewModel: DSBridgesVM!
    var dataCallBack: ((_ success: Bool, _ data: String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        weak var weakSelf: DSScanQRVC! = self
        DispatchQueue.main.async {
            weakSelf.view.backgroundColor = .white
            let _: UIView = DSNavigationView(title: "Scan QR Code", controller: weakSelf)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startReading()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        stopReading()
    }
    
    // MARK: AVCaptureMetadataOutputObjectsDelegate

    /**
    BUGFIX: Signature of method changed in Swift 4, without notifications.
    No migration assistance either.

    See https://stackoverflow.com/questions/46639519/avcapturemetadataoutputobjectsdelegate-not-called-in-swift-4-for-qr-scanner
    */
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count > 0, let metadata = metadataObjects[0] as? AVMetadataMachineReadableCodeObject, metadata.type == .qr {
            viewModel.tryDecode(metadata.stringValue) { success, data in
                if let cb = dataCallBack {
                    cb(success, data)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                        self?.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }


    // MARK: Private Methods

    private func startReading() {

        if let captureDevice = AVCaptureDevice.default(for: .video) {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)

                captureSession = AVCaptureSession()

                captureSession!.addInput(input)

                let captureMetadataOutput = AVCaptureMetadataOutput()
                captureSession!.addOutput(captureMetadataOutput)
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [.qr]

                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)

                videoPreviewLayer!.videoGravity = .resizeAspectFill
                videoPreviewLayer!.frame = view.layer.bounds
                view.layer.addSublayer(videoPreviewLayer!)

                captureSession!.startRunning()

                return
            } catch {
                // Just fall thru to alert.
            }
        }

        let warning = UILabel(frame: .zero)
        warning.text = "Camera access was not granted or QR Code scanning is not supported by your device."
        warning.translatesAutoresizingMaskIntoConstraints = false
        warning.numberOfLines = 0
        warning.textAlignment = .center
        warning.textColor = .black

        view.addSubview(warning)
        NSLayoutConstraint.activate([
            warning.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15),
            warning.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15),
            warning.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    private func stopReading() {
        captureSession?.stopRunning()
        captureSession = nil

        videoPreviewLayer?.removeFromSuperlayer()
        videoPreviewLayer = nil
    }
}
