//
//  DSBridgesVM.swift
//  DSTor
//
//  Created by Macbook on 13/07/2021.
//

import UIKit

class DSBridgesVM: NSObject {
    
    struct BridgeMap {
        var type: Configrations.TorBridgeKind
        var strType: String
        
        init(_ type: Configrations.TorBridgeKind, _ strType: String) {
            self.type = type
            self.strType = strType
        }
    }
    
    static var section1Data: [BridgeMap] = {
        return [
            BridgeMap(.none, "No Bridges"),
            BridgeMap(.obfs4, "Built-in obfs4"),
            BridgeMap(.snowflake, "Built-in snowflake"),
            BridgeMap(.custom, "Custom Bridges")
        ]
    }()
    
    lazy var imagePickerView: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        return picker
    }()
    
    lazy var detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
    
    lazy var bridgesUrl: String = "https://bridges.torproject.org/"
    
    var currentBridgesType: Configrations.TorBridgeKind

    var customBridges: [String]?
    
    let placeHolderTextForCustomBridges: String = DSTorManager.obfs4Bridges.first ?? "Paste Bridges that were copied from Browser"

    func connect() {
        Configrations.recentUsedTorBrigeType = currentBridgesType
        Configrations.ownBridges = customBridges

        // At this point we already have a connection. The bridge reconfiguration is very cheap,
        // so we stay in the browser view and let OnionManager reconfigure in the background.
        // Actually, the reconfiguration can be done completely offline, so we don't have a chance to
        // find out, if another bridge setting (or no bridge) actually works afterwards.
        // The user will find out, when she tries to continue browsing.

        DSTorManager.shared.setBridgeSettings(bridgesType: Configrations.recentUsedTorBrigeType, customBridges: Configrations.ownBridges)
        DSTorManager.shared.startTorBrowser(delegate: nil)
    }

    func tryDecode(_ raw: String?, with callBack: ((_ success: Bool, _ data: String) -> Void)){
        // They really had to use JSON for content encoding but with illegal single quotes instead
        // of double quotes as per JSON standard. Srsly?
        if let data = raw?.replacingOccurrences(of: "'", with: "\"").data(using: .utf8),
            let newBridges = try? JSONSerialization.jsonObject(with: data, options: []) as? [String] {

            callBack(true, newBridges.joined(separator: "\n"))
        }
        else {
           callBack(false, "")
        }
    }
    
    override init() {
        currentBridgesType = Configrations.recentUsedTorBrigeType
        customBridges = Configrations.ownBridges
        
        super.init()
    }
    
}
