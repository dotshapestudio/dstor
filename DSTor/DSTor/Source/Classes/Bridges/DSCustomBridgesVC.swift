//
//  DSCustomBridgesVC.swift
//  DSTor
//
//  Created by Macbook on 14/07/2021.
//

import UIKit

class DSCustomBridgesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private let tableView: UITableView = UITableView()
    private let textField: UITextView = UITextView()
    private let theme_color: UIColor = .getColor(.DSPurple)
    var viewModel: DSBridgesVM!
    var customBridgesSelected: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        weak var weakSelf: DSCustomBridgesVC? = self
        DispatchQueue.main.async {
            weakSelf?.generateUI()
        }
    }

    // MARK: UI Private Functions
    
    private func addTableView(topView: UIView) {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: .zero)
    
        self.view.addSubview(tableView)
        
        // MARK: TableView Constarints
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: topView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    // MARK: UI Generation
    
    private func generateUI() {
        self.view.backgroundColor = .white
        let top: UIView = DSNavigationView(title: "Custom Bridges Configration", controller: self)
        addTableView(topView: top)
    }
    
    // MARK: Cell UI Creation
    private func getTableViewCell() -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell()
        
        let copyUrlView: UIView = getCopyURLView()
        cell.contentView.addSubview(copyUrlView)
        
        let pasteUrlView: UIView = getPasteBridgesView()
        cell.contentView.addSubview(pasteUrlView)
        
        let qrUrlView: UIView = getQRReaderView()
        cell.contentView.addSubview(qrUrlView)
        
        let connectView: UIView = getConnectBridgeView()
        cell.contentView.addSubview(connectView)
        
        // Copy URL Constraints
        NSLayoutConstraint.activate([
            copyUrlView.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor),
            copyUrlView.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor),
            copyUrlView.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 25),
            copyUrlView.bottomAnchor.constraint(equalTo: pasteUrlView.topAnchor, constant: -25)
        ])
        
        // Paste URL Constraint
        NSLayoutConstraint.activate([
            pasteUrlView.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor),
            pasteUrlView.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor),
            pasteUrlView.bottomAnchor.constraint(equalTo: qrUrlView.topAnchor, constant: -25)
        ])
        
        // QR URL Constraint
        NSLayoutConstraint.activate([
            qrUrlView.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor),
            qrUrlView.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor),
            qrUrlView.bottomAnchor.constraint(equalTo: connectView.topAnchor, constant: -25)
        ])
        
        // Connect View Constraint
        NSLayoutConstraint.activate([
            connectView.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor),
            connectView.rightAnchor.constraint(equalTo: cell.contentView.rightAnchor),
            connectView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -25)
        ])
        
        cell.selectionStyle = .none
        return cell
    }
    
    private func getCopyURLView() -> UIView {
        let urlView: UIView = getView()
        
        let title: UILabel = getHeadingLabel(with: "In a separate browser, visit \(viewModel.bridgesUrl) and tap \"Get Bridges\" > \"Just Give Me Bridges!\"")
        let action: UIButton = getButtonAction(with: "Copy URL to Clipboard")
        action.addTarget(self, action: #selector(copyURLClicked(_:)), for: .touchUpInside)
        
        urlView.addSubview(title)
        urlView.addSubview(action)
        
        // title Constraint
        NSLayoutConstraint.activate([
            title.leftAnchor.constraint(equalTo: urlView.leftAnchor, constant: 8),
            title.rightAnchor.constraint(equalTo: urlView.rightAnchor, constant: -8),
            title.topAnchor.constraint(equalTo: urlView.topAnchor, constant: 8),
            title.bottomAnchor.constraint(equalTo: action.topAnchor, constant: -16)
        ])
        
        // action Constraint
        NSLayoutConstraint.activate([
            action.centerXAnchor.constraint(equalTo: urlView.centerXAnchor),
            action.bottomAnchor.constraint(equalTo: urlView.bottomAnchor, constant: -8)
        ])
        
        return urlView
    }
    
    private func getPasteBridgesView() -> UIView {
        let pasteView: UIView = getView()
        
        let title: UILabel = getHeadingLabel(with: "Paste Bridges")
        let textView: UITextView = getTextArea(with: viewModel.customBridges)
        
        pasteView.addSubview(title)
        pasteView.addSubview(textView)
        
        // title Constraint
        NSLayoutConstraint.activate([
            title.leftAnchor.constraint(equalTo: pasteView.leftAnchor, constant: 8),
            title.rightAnchor.constraint(equalTo: pasteView.rightAnchor, constant: -8),
            title.topAnchor.constraint(equalTo: pasteView.topAnchor, constant: 8),
            title.bottomAnchor.constraint(equalTo: textView.topAnchor, constant: -16)
        ])
        
        // action Constraint
        NSLayoutConstraint.activate([
            textView.leftAnchor.constraint(equalTo: pasteView.leftAnchor),
            textView.rightAnchor.constraint(equalTo: pasteView.rightAnchor),
            textView.heightAnchor.constraint(equalToConstant: 120),
            textView.bottomAnchor.constraint(equalTo: pasteView.bottomAnchor, constant: -8)
        ])
        
        return pasteView
    }
    
    private func getQRReaderView() -> UIView {
        let qrView = getView()
        
        let title: UILabel = getHeadingLabel(with: "Use QR Code")
        let scanQR: UIButton = getButtonAction(with: "Scan QR Code")
        scanQR.addTarget(self, action: #selector(scanQRClicked(_:)), for: .touchUpInside)
        let uploadQR: UIButton = getButtonAction(with: "Upload QR Code")
        uploadQR.addTarget(self, action: #selector(uploadQRClicked(_:)), for: .touchUpInside)
        
        qrView.addSubview(title)
        qrView.addSubview(scanQR)
        qrView.addSubview(uploadQR)
        
        // title Constraint
        NSLayoutConstraint.activate([
            title.leftAnchor.constraint(equalTo: qrView.leftAnchor, constant: 8),
            title.rightAnchor.constraint(equalTo: qrView.rightAnchor, constant: -8),
            title.topAnchor.constraint(equalTo: qrView.topAnchor, constant: 8),
            title.bottomAnchor.constraint(equalTo: scanQR.topAnchor, constant: -16)
        ])
        
        // scanQR constraints
        NSLayoutConstraint.activate([
            scanQR.centerXAnchor.constraint(equalTo: qrView.centerXAnchor),
            scanQR.bottomAnchor.constraint(equalTo: uploadQR.topAnchor, constant: -8)
        ])
        
        // uploadQR Constraint
        NSLayoutConstraint.activate([
            uploadQR.centerXAnchor.constraint(equalTo: qrView.centerXAnchor),
            uploadQR.bottomAnchor.constraint(equalTo: qrView.bottomAnchor, constant: -8)
        ])
        
        return qrView
    }
    
    private func getConnectBridgeView() -> UIView {
        let connectView: UIView = getView()
        
        let title: UILabel = getHeadingLabel(with: "Tap the button to connect the Bridges")
        let action: UIButton = getButtonAction(with: "Connect")
        action.addTarget(self, action: #selector(connectClicked(_:)), for: .touchUpInside)
        
        connectView.addSubview(title)
        connectView.addSubview(action)
        
        // title Constraint
        NSLayoutConstraint.activate([
            title.leftAnchor.constraint(equalTo: connectView.leftAnchor, constant: 8),
            title.rightAnchor.constraint(equalTo: connectView.rightAnchor, constant: -8),
            title.topAnchor.constraint(equalTo: connectView.topAnchor, constant: 8),
            title.bottomAnchor.constraint(equalTo: action.topAnchor, constant: -16)
        ])
        
        // action Constraint
        NSLayoutConstraint.activate([
            action.centerXAnchor.constraint(equalTo: connectView.centerXAnchor),
            action.bottomAnchor.constraint(equalTo: connectView.bottomAnchor, constant: -8)
        ])
        
        return connectView
    }
    
    private func getHeadingLabel(with text: String) -> UILabel {
        let lbl: UILabel = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textAlignment = .left
        lbl.textColor = .lightGray
        lbl.font = .systemFont(ofSize: 12)
        lbl.numberOfLines = 0
        lbl.text = text
        
        return lbl
    }
    
    private func getButtonAction(with text: String) -> UIButton {
        let btn: UIButton = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle(text, for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = theme_color
        btn.titleLabel?.font = .boldSystemFont(ofSize: 14)
        btn.layer.cornerRadius = 17.5
        btn.layer.masksToBounds = true
        
        // Constraints
        NSLayoutConstraint.activate([
            btn.widthAnchor.constraint(equalToConstant: 250),
            btn.heightAnchor.constraint(equalToConstant: 35)
        ])
        
        return btn
    }
    
    private func getView() -> UIView {
        let view: UIView = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }
    
    private func getTextArea(with text: [String]?) -> UITextView {
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.showsHorizontalScrollIndicator = false
        textField.showsVerticalScrollIndicator = false
        textField.isScrollEnabled = true
        textField.isEditable = true
        textField.isSelectable = true
        textField.textAlignment = .left
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.textContentType = .name
        textField.smartDashesType = .no
        textField.smartInsertDeleteType = .no
        textField.smartQuotesType = .no
        textField.spellCheckingType = .no
        textField.keyboardType = .default
        textField.returnKeyType = .default
        textField.backgroundColor = .white
        textField.font = UIFont.systemFont(ofSize: 16)
        textField.textColor = theme_color
        if let txt = text {
            textField.text = txt.joined(separator: "\n")
        }
        textField.placeholder = viewModel.placeHolderTextForCustomBridges // always add placeholder after text for correct functioning
        
        
        return textField
    }
    
    // MARK: UITableView Delegates and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getTableViewCell()
    }
    
    // MARK: Actions
    
    @IBAction private func copyURLClicked(_ sender: Any) {
        UIPasteboard.general.string = viewModel.bridgesUrl
    }
    
    @IBAction private func scanQRClicked(_ sender: Any) {
        let scanQR: DSScanQRVC = DSScanQRVC()
        scanQR.modalPresentationStyle = .formSheet
        weak var weakSelf: DSCustomBridgesVC? = self
        scanQR.dataCallBack = { success, data in
            if success {
                weakSelf?.textField.setText(text: data)
            } else {
                self.showAlert(title: "Error", message: "QR Code could not be decoded! Are you sure you scanned a QR code from camera?")
            }
        }
        scanQR.viewModel = viewModel
        
        self.present(scanQR, animated: true)
    }
    
    @IBAction private func uploadQRClicked(_ sender: Any) {
        let imagePicker: UIImagePickerController = viewModel.imagePickerView
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .formSheet
        self.present(imagePicker, animated: true)
    }
    
    @IBAction private func connectClicked(_ sender: Any) {
        let text: String = textField.text
        if text.isEmpty {
            self.showAlert(title: "Error", message: "Please add bridges first")
            return
        }
        
        viewModel.customBridges = text.components(separatedBy: "\n").map({ bridge in bridge.trimmingCharacters(in: .whitespacesAndNewlines) }).filter({ bridge in !bridge.isEmpty && !bridge.hasPrefix("//") && !bridge.hasPrefix("#") })

        viewModel.currentBridgesType = .custom
        viewModel.connect()
        
        if let cb = customBridgesSelected {
            cb()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    var controller = UIApplication.shared.windows.first!.rootViewController
                    while controller?.presentedViewController != nil {
                        controller = controller!.presentedViewController
                    }
                    controller?.dismiss(animated: true)
                }
            })
        }
    }
    
    // MARK: UIImagePickerController Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        var raw = ""

        if let image = (info[.editedImage] ?? info[.originalImage]) as? UIImage,
            let ciImage = image.ciImage ?? (image.cgImage != nil ? CIImage(cgImage: image.cgImage!) : nil) {

            let features = viewModel.detector?.features(in: ciImage)

            for feature in features as? [CIQRCodeFeature] ?? [] {
                raw += feature.messageString ?? ""
            }
        }

        weak var weakSelf: DSCustomBridgesVC? = self
        viewModel.tryDecode(raw) { success, data in
            if success {
                weakSelf?.textField.setText(text: data)
            } else {
                self.showAlert(title: "Error", message: "QR Code could not be decoded! Are you sure you scanned a QR code from \(String(describing: weakSelf?.viewModel.bridgesUrl))?")
            }
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
}
