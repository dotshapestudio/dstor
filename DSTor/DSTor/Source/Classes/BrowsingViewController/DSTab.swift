//
//  Tab.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 22.11.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit
import QuickLook

protocol DSTabProtocol: class {
	func updateProgress()

	func insertTab(_ url: URL?) -> DSTab?

	func insertTab(_ url: URL?, forRestoration: Bool,
				   transition: DSMainWebController.Animation,
				   completion: ((Bool) -> Void)?) -> DSTab?

	func deleteTab(_ tab: DSTab, focus: DSTab?)

	func tab(ipcId: String?) -> DSTab?

	func tab(hash: Int?) -> DSTab?

	func index(of tab: DSTab) -> Int?

	func present(_ vc: UIViewController, _ sender: UIView?)

	func resignFRToSearchField()
}

class DSTab: UIView {

	@objc
	enum ProtectionMode: Int {
		case insecure
		case mixed
		case secure
		case secureEv
	}


	weak var delegete: DSTabProtocol?

	var name: String {
		if let downloadedFile = fileDownloaded {
			return downloadedFile.lastPathComponent
		}

		if let title = stringByTestingJS(from: "document.title") {
			if !title.isEmpty {
				return title
			}
		}

		return DSMainWebController.prettyTitle(link)
	}

	var idSuper: Int?

	var idIPC: String?

	@objc
	var link = URL.start

	@objc
	var index: Int {
		return delegete?.index(of: self) ?? -1
	}

	private(set) var shouldReload = false

	@objc(httpEverywhereRuleApply)
	var httpEverywhereRuleApply = NSMutableDictionary()

	@objc(urlBlockerTargetsApply)
	var urlBlockerTargetsApply = NSMutableDictionary()

	@objc(CertificateSSL)
	var certificateSSL: SSLCertificate? {
		didSet {
			if certificateSSL == nil {
				protectionMode = .insecure
			}
			else if certificateSSL?.isEV ?? false {
				protectionMode = .secureEv
			}
			else {
				protectionMode = .secure
			}
		}
	}

	@objc
	var protectionMode = ProtectionMode.insecure

	@nonobjc
	var progress: Float = 0 {
		didSet {
			DispatchQueue.main.async {
				self.delegete?.updateProgress()
			}
		}
	}

	static let sizeForHistory = 40
	var historySkip = false

//	var history = [HistoryViewController.Item]()

	override var isUserInteractionEnabled: Bool {
		didSet {
			if qlPrevController != nil {
				if isUserInteractionEnabled {
					cover.removeFromSuperview()
				}
				else {
					cover.add(to: self)
				}
			}
		}
	}

	private(set) lazy var browser: UIWebView = {
		let view = UIWebView()

		view.delegate = self
		view.scalesPageToFit = true
		view.allowsInlineMediaPlayback = true

		return view.add(to: self)
	}()

	var scroll: UIScrollView {
		return browser.scrollView
	}

	var browserCanGoBack: Bool {
		return  idSuper != nil || browser.canGoBack
	}

	var browserCanGoForward: Bool {
		return browser.canGoForward
	}

	var isWaiting: Bool {
		return browser.isLoading
	}

	var qlPrevController: QLPreviewController?

	/**
	Add another overlay (a hack to create a transparant clickable view)
	to disable interaction with the file preview when used in the tab overview.
	*/
	private(set) lazy var cover: UIView = {
		let view = UIView()
		view.backgroundColor = .white
		view.alpha = 0.11
		view.isUserInteractionEnabled = false

		return view
	}()

	var fileDownloaded: URL?

	private(set) lazy var reloader: UIRefreshControl = {
		let refresher = UIRefreshControl()

		refresher.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to Refresh Page", comment: ""))

		return refresher
	}()


	init(restorationId: String?) {
		super.init(frame: .zero)

		setup(restorationId)
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)

		setup()
	}


	// MARK: Public Methods

	@objc
	func reload() {
		if link == URL.start {
            print("commented: Bookmark.updateStartPage()")
//			Bookmark.updateStartPage()
		}

		shouldReload = false
		historySkip = true
		browser.reload()
	}

	func hold() {
		browser.stopLoading()
	}

	@objc
	func load(_ url: URL?) {
		var request: URLRequest?

		if let url = url?.alongFixedScheme?.actual {
			request = URLRequest(url: url)
		}

		load(request)
	}

	func load(_ request: URLRequest?) {
		DispatchQueue.main.async {
			self.browser.stopLoading()
		}

		reset()

		let request = request ?? URLRequest(url: URL.start)

		if let url = request.url {
			if url == URL.start {
				//Bookmark.updateStartPage()
			}

			self.link = url
		}

		DispatchQueue.main.async {
			self.browser.loadRequest(request)
		}
	}

	@objc
	func find(for query: String?) {
		return load(BrowsingHistoryVC.makeRequest(query))
	}

	func reset(_ url: URL? = nil) {
		httpEverywhereRuleApply.removeAllObjects()
		urlBlockerTargetsApply.removeAllObjects()
		certificateSSL = nil
		self.link = url ?? URL.start
	}

	@objc
	func browserGoPrev() {
		if browser.canGoBack {
			historySkip = true
			browser.goBack()
		}
		else if let parentId = idSuper {
			delegete?.deleteTab(self, focus: delegete?.tab(hash: parentId))
		}
	}

	@objc
	func browserGoNext() {
		if browser.canGoForward {
			historySkip = true
			browser.goForward()
		}
	}

	@discardableResult
	func stringByTestingJS(from script: String) -> String? {
		return browser.stringByEvaluatingJavaScript(from: script)
	}

	/**
	Call this before giving up the tab, otherwise memory leaks will occur!
	*/
	func dismiss() {
		dismissDownload()

		let block = {
			NotificationCenter.default.removeObserver(self)

			self.delegete = nil
			self.scroll.delegate = nil
			self.browser.delegate = nil

			for gr in self.browser.gestureRecognizers ?? [] {
				self.browser.removeGestureRecognizer(gr)
			}

			self.hold()
			self.browser.loadHTMLString("", baseURL: nil)

			self.browser.removeFromSuperview()
			self.removeFromSuperview()
		}

		if Thread.isMainThread {
			block()
		}
		else {
			DispatchQueue.main.sync(execute: block)
		}
	}

	func clear() {
		let block = {
			self.hold()

			// Will empty the webView, but keep the URL and doesn't create a history entry.
			self.stringByTestingJS(from: "document.open()")
			
			self.shouldReload = true
		}

		if Thread.isMainThread {
			block()
		}
		else {
			DispatchQueue.main.sync(execute: block)
		}
	}


	// MARK: Private Methods

	private func setup(_ restorationId: String? = nil) {
		// Re-register user agent with our hash, which should only affect this UIWebView.
		UserDefaults.standard.register(defaults: ["UserAgent": "\(AppDelegate.shared?.defaultUserAgent ?? "")/\(hash)"])

		if restorationId != nil {
			restorationIdentifier = restorationId
			shouldReload = true
		}

		NotificationCenter.default.addObserver(
			self, selector: #selector(estimatedProgressChanged(_:)),
			name: NSNotification.Name(rawValue: "WebProgressEstimateChangedNotification"),
			object: browser.value(forKeyPath: "documentView.webView"))

		// Immediately refresh the page if its host settings were changed, so
		// users sees the impact of their changes.
		NotificationCenter.default.addObserver(self, selector: #selector(hostConfigrationsChanged(_:)),
											   name: .hostSettingsReplaced, object: nil)

		// This doubles as a way to force the webview to initialize itself,
		// otherwise the UA doesn't seem to set right before refreshing a previous
		// restoration state.
		let hashInUa = stringByTestingJS(from: "navigator.userAgent")?.split(separator: "/").last

		if hashInUa?.compare(String(hash)) != ComparisonResult.orderedSame {
			print("[Tab \(index)] Aborting, not equal! hashInUa=\(String(describing: hashInUa)), hash=\(hash)")
			abort()
		}

//		setupGestureRecognizers()
	}

	@objc
	private func estimatedProgressChanged(_ notification: Notification) {
		progress = Float(notification.userInfo?["WebProgressEstimatedProgressKey"] as? Float ?? 0)
	}

	@objc
	private func hostConfigrationsChanged(_ notification: Notification) {
		let host = notification.object as? String

		// Refresh on default changes and specific changes for this host.
		if host == nil || host == self.link.host {
			self.reload()
		}
	}


	deinit {
		dismiss()
	}
}
