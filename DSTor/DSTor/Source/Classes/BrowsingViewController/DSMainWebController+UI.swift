//
//  DSMainWebController+UI.swift
//  DSTor
//
//  Created by Macbook on 19/07/2021.
//

import Foundation

extension DSMainWebController {
    
    // MARK: UI Generation
    
    func generateUI() {
        view.backgroundColor = UIColor.white
        headerView()
        progressView()
        addHeaderView()
        addColectionView()
        addTabBarView()
        addContainerView()
    }
    
    private func addColectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 25
        layout.sectionInset = UIEdgeInsets(top: 8, left: 25, bottom: 8, right: 25)
        layout.sectionInsetReference = .fromContentInset
        layout.minimumInteritemSpacing = 25
        
        collectionViewForTabs = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionViewForTabs.translatesAutoresizingMaskIntoConstraints = false
        collectionViewForTabs.delegate = self
        collectionViewForTabs.dataSource = self
        collectionViewForTabs.dragDelegate = self
        collectionViewForTabs.dropDelegate = self
        collectionViewForTabs.isScrollEnabled = true
        collectionViewForTabs.showsVerticalScrollIndicator = false
        collectionViewForTabs.showsHorizontalScrollIndicator = false
        collectionViewForTabs.bouncesZoom = true
        collectionViewForTabs.bounces = true
        collectionViewForTabs.backgroundColor = UIColor(red: 111 / 255, green: 113 / 255, blue: 121 / 255, alpha: 1)
        collectionViewForTabs.isOpaque = true
        collectionViewForTabs.isHidden = true
        collectionViewForTabs.clipsToBounds = true
        collectionViewForTabs.clearsContextBeforeDrawing = true
        collectionViewForTabs.autoresizesSubviews = true
        collectionViewForTabs.register(TabCell.self, forCellWithReuseIdentifier: "cell")
        
        view.addSubview(collectionViewForTabs)
        
        // Constraints
        NSLayoutConstraint.activate([
            collectionViewForTabs.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionViewForTabs.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionViewForTabs.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionViewForTabs.topAnchor.constraint(equalTo: headerViewForAddTab.bottomAnchor)
        ])
    }
    
    private func addHeaderView() {
        headerViewForAddTab.translatesAutoresizingMaskIntoConstraints = false
        headerViewForAddTab.backgroundColor = UIColor.getColor(.DSPurple)
        headerViewForAddTab.isHidden = true
        
        view.addSubview(headerViewForAddTab)
        
        // TabsHeaderView Constraint
        NSLayoutConstraint.activate([
            headerViewForAddTab.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerViewForAddTab.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            headerViewForAddTab.topAnchor.constraint(equalTo: view.topAnchor),
            headerViewForAddTab.heightAnchor.constraint(equalToConstant: 80)
        ])
        
        let btnDone: UIButton = getHeaderViewButton(withFont: .systemFont(ofSize: 17), andTitle: "Done")
        btnDone.addTarget(self, action: #selector(hideTabsScreen), for: .touchUpInside)
        
        let btnPlus: UIButton = getHeaderViewButton(withFont: .systemFont(ofSize: 30), andTitle: "+")
        btnPlus.addTarget(self, action: #selector(newTabFromTabsScreen), for: .touchUpInside)
        
        headerViewForAddTab.addSubview(btnDone)
        headerViewForAddTab.addSubview(btnPlus)
        
        // Button's Constraints
        NSLayoutConstraint.activate([
            btnPlus.bottomAnchor.constraint(equalTo: headerViewForAddTab.bottomAnchor),
            btnPlus.centerXAnchor.constraint(equalTo: headerViewForAddTab.centerXAnchor),
            btnDone.centerYAnchor.constraint(equalTo: btnPlus.centerYAnchor),
            btnDone.leadingAnchor.constraint(equalTo: headerViewForAddTab.leadingAnchor, constant: 25)
        ])
    }
    
    private func getHeaderViewButton(withFont font: UIFont, andTitle text: String) -> UIButton {
        let btn: UIButton = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle(text, for: .normal)
        btn.contentVerticalAlignment = .center
        btn.contentHorizontalAlignment = .center
        btn.titleLabel!.font = font
        btn.setTitleColor(.white, for: .normal)
        
        return btn
    }
    
    private func addContainerView() {
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .systemBackground
        
        view.addSubview(container)
        
        NSLayoutConstraint.activate([
            container.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            container.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            container.topAnchor.constraint(equalTo: progBar.bottomAnchor),
            container.bottomAnchor.constraint(equalTo: bottomView.topAnchor)
        ])
    }
    
    private func addTabBarView() {
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = .systemBackground
        
        view.addSubview(bottomView)
        
        let bottomSafeSpace = view.safeAreaInsets.bottom
        
        
        // tabBarView Constraint
        NSLayoutConstraint.activate([
            bottomView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            bottomView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomView.heightAnchor.constraint(equalToConstant: 60 + bottomSafeSpace)
        ])
        
        let lineToDraw: UIView = line(to: bottomView)
        let stackView: UIStackView = getButtonsStackView()
        
        bottomView.addSubview(lineToDraw)
        bottomView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor, constant: -bottomSafeSpace),
            stackView.topAnchor.constraint(equalTo: lineToDraw.bottomAnchor)
        ])
    }
    
    private func line(to outerView: UIView) -> UIView {
        let line: UIView = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = UIColor.getColor(.DSPurple)
        
        outerView.addSubview(line)
        
        // line Constraints
        NSLayoutConstraint.activate([
            line.topAnchor.constraint(equalTo: outerView.topAnchor),
            line.leadingAnchor.constraint(equalTo: outerView.leadingAnchor),
            line.trailingAnchor.constraint(equalTo: outerView.trailingAnchor),
            line.heightAnchor.constraint(equalToConstant: 1)
        ])
        
        return line
    }
    
    private func getButtonsStackView() -> UIStackView {
        let stView: UIStackView = UIStackView()
        stView.translatesAutoresizingMaskIntoConstraints = false
        stView.backgroundColor = .clear
        stView.alignment = .fill
        stView.distribution = .fillEqually
        stView.axis = .horizontal
        
        btnToMoveBackward = getStackViewButton(withImage: UIImage(systemName: "arrow.left"))
        btnToMoveForward = getStackViewButton(withImage: UIImage(systemName: "arrow.right"))
        btnShowBookmark = getStackViewButton(withImage: UIImage(systemName: "bookmark"))
        btnTabsOption = getStackViewButton(withImage: UIImage(systemName: "square.on.square"))
        btnToViewCircuits = getStackViewButton(withImage: UIImage(named: "Onion-Icon"))
        
        stView.addArrangedSubview(btnToMoveBackward)
        stView.addArrangedSubview(btnToMoveForward)
        stView.addArrangedSubview(btnShowBookmark)
        stView.addArrangedSubview(btnTabsOption)
        stView.addArrangedSubview(btnToViewCircuits)
        
        return stView
    }
    
    private func getStackViewButton(withImage img: UIImage?) -> UIButton {
        let btn: UIButton = UIButton()
        btn.contentVerticalAlignment = .center
        btn.contentHorizontalAlignment = .center
        btn.setImage(img, for: .normal)
        btn.tintColor = UIColor.getColor(.DSPurple)
        btn.scalesLargeContentImage = true
        btn.setPreferredSymbolConfiguration(UIImage.SymbolConfiguration(pointSize: 17, weight: .regular, scale: .large), forImageIn: .normal)
        btn.addTarget(self, action: #selector(buttonsListener), for: .touchUpInside)
        
        
        return btn
    }
    
    private func headerView() {
        topSearchView.translatesAutoresizingMaskIntoConstraints = false
        topSearchView.backgroundColor = UIColor.getColor(.DSPurple)
        view.addSubview(topSearchView)
        
        NSLayoutConstraint.activate([
            topSearchView.topAnchor.constraint(equalTo: view.topAnchor),
            topSearchView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            topSearchView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topSearchView.heightAnchor.constraint(equalToConstant: 80),
        ])
        
        createSearchTF()
        createMenuBtn()
    }
    
    private func createMenuBtn() {
        btnSettings.translatesAutoresizingMaskIntoConstraints = false
        btnSettings.setImage(UIImage.init(systemName: "gearshape"), for: .normal)
        btnSettings.tintColor = .white
        btnSettings.addTarget(self, action: #selector(buttonsListener), for: .touchUpInside)
        
        topSearchView.addSubview(btnSettings)
        
        NSLayoutConstraint.activate([
            btnSettings.trailingAnchor.constraint(equalTo: topSearchView.trailingAnchor, constant: -5),
            btnSettings.widthAnchor.constraint(equalToConstant: 34),
            btnSettings.topAnchor.constraint(equalTo: searchFl.topAnchor),
            btnSettings.bottomAnchor.constraint(equalTo: searchFl.bottomAnchor),
            btnSettings.leadingAnchor.constraint(equalTo: searchFl.trailingAnchor, constant: 5)
        ])
    }
    
    private func createSearchTF() {
        searchFl = UITextField()
        searchFl.translatesAutoresizingMaskIntoConstraints = false
        searchFl.backgroundColor = UIColor.white
        searchFl.delegate = self
        searchFl.borderStyle = .roundedRect
        searchFl.textColor = .black
        searchFl.minimumFontSize = 17
        searchFl.clearButtonMode = .whileEditing
        searchFl.textAlignment = .natural
        searchFl.textContentType = .none
        searchFl.autocapitalizationType = .none
        searchFl.autocorrectionType = .no
        searchFl.smartDashesType = .no
        searchFl.smartQuotesType = .no
        searchFl.spellCheckingType = .no
        searchFl.keyboardType = .webSearch
        searchFl.keyboardAppearance = .default
        searchFl.returnKeyType = .go
        searchFl.addTarget(self, action: #selector(textFieldTextChanged), for: .editingChanged)
        
        topSearchView.addSubview(searchFl)
        
        
        
        NSLayoutConstraint.activate([
            searchFl.leadingAnchor.constraint(equalTo: topSearchView.leadingAnchor , constant: 15),
            searchFl.bottomAnchor.constraint(equalTo: topSearchView.bottomAnchor, constant: -8),
            searchFl.heightAnchor.constraint(equalToConstant: 34)
        ])
    }
    
    private func progressView() {
        progBar.translatesAutoresizingMaskIntoConstraints = false
        progBar.tintColor = UIColor.black
        progBar.progressTintColor = .black
        progBar.trackTintColor = .white
        progBar.backgroundColor = UIColor.white
        progBar.progress = 0
        
        view.addSubview(progBar)
        
        NSLayoutConstraint.activate([
            progBar.topAnchor.constraint(equalTo: topSearchView.bottomAnchor),
            progBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            progBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
}
