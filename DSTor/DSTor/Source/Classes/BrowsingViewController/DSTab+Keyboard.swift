//
//  Tab+Keyboard.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 21.11.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import Foundation

/**
Encapsulates all keyboard handling of a `Tab`.
*/
extension DSTab {

	/**
	A Keyboard Map Entry.
	*/
	private struct KeyboardMapEntry {
		let keycode: Int
		let keypressKeycode: Int
		let shiftKeycode: Int

		init(_ keycode: Int, _ keypressKeycode: Int, _ shiftKeycode: Int) {
			self.keycode = keycode
			self.keypressKeycode = keypressKeycode
			self.shiftKeycode = shiftKeycode
		}

		init(_ keycode: Int, _ keypressKeycode: Character, _ shiftKeycode: Character) {
			self.init(keycode, Int(keypressKeycode.asciiValue ?? 0), Int(shiftKeycode.asciiValue ?? 0))
		}

		init(_ keycode: Character, _ keypressKeycode: Character, _ shiftKeycode: Character) {
			self.init(Int(keycode.asciiValue ?? 0), keypressKeycode, shiftKeycode)
		}
	}

	private static let mapKeyboard: [String: KeyboardMapEntry] = [
		UIKeyCommand.inputEscape: KeyboardMapEntry(27, 0, 0),

		"`": KeyboardMapEntry(192, "`", "~"),
		"1": KeyboardMapEntry("1", "1", "!"),
		"2": KeyboardMapEntry("2", "2", "@"),
		"3": KeyboardMapEntry("3", "3", "#"),
		"4": KeyboardMapEntry("4", "4", "$"),
		"5": KeyboardMapEntry("5", "5", "%"),
		"6": KeyboardMapEntry("6", "6", "^"),
		"7": KeyboardMapEntry("7", "7", "&"),
		"8": KeyboardMapEntry("8", "8", "*"),
		"9": KeyboardMapEntry("9", "9", "("),
		"0": KeyboardMapEntry("0", "0", ")"),
		"-": KeyboardMapEntry(189, "-", "_"),
		"=": KeyboardMapEntry(187, "=", "+"),
		"\u{8}": KeyboardMapEntry(8, 0, 0),

		"\t": KeyboardMapEntry(9, 0, 0),
		"q": KeyboardMapEntry("Q", "q", "Q"),
		"w": KeyboardMapEntry("W", "w", "W"),
		"e": KeyboardMapEntry("E", "e", "E"),
		"r": KeyboardMapEntry("R", "r", "R"),
		"t": KeyboardMapEntry("T", "t", "T"),
		"y": KeyboardMapEntry("Y", "y", "Y"),
		"u": KeyboardMapEntry("U", "u", "U"),
		"i": KeyboardMapEntry("I", "i", "I"),
		"o": KeyboardMapEntry("O", "o", "O"),
		"p": KeyboardMapEntry("P", "p", "P"),
		"[": KeyboardMapEntry(219, "[", "{"),
		"]": KeyboardMapEntry(221, "]", "}"),
		"\\": KeyboardMapEntry(220, "\\", "|"),

		"a": KeyboardMapEntry("A", "a", "A"),
		"s": KeyboardMapEntry("S", "s", "S"),
		"d": KeyboardMapEntry("D", "d", "D"),
		"f": KeyboardMapEntry("F", "f", "F"),
		"g": KeyboardMapEntry("G", "g", "G"),
		"h": KeyboardMapEntry("H", "h", "H"),
		"j": KeyboardMapEntry("J", "j", "J"),
		"k": KeyboardMapEntry("K", "k", "K"),
		"l": KeyboardMapEntry("L", "l", "L"),
		";": KeyboardMapEntry(186, ";", ":"),
		"'": KeyboardMapEntry(222, "'", "\""),
		"\r": KeyboardMapEntry(13, 0, 0),

		"z": KeyboardMapEntry("Z", "z", "Z"),
		"x": KeyboardMapEntry("X", "x", "X"),
		"c": KeyboardMapEntry("C", "c", "C"),
		"v": KeyboardMapEntry("V", "v", "V"),
		"b": KeyboardMapEntry("B", "b", "B"),
		"n": KeyboardMapEntry("N", "n", "N"),
		"m": KeyboardMapEntry("M", "m", "M"),
		",": KeyboardMapEntry(188, ",", "<"),
		".": KeyboardMapEntry(190, ".", ">"),
		"/": KeyboardMapEntry(191, "/", "/"),

		" ": KeyboardMapEntry(" ", " ", " "),
		UIKeyCommand.inputLeftArrow: KeyboardMapEntry(37, 0, 0),
		UIKeyCommand.inputUpArrow: KeyboardMapEntry(38, 0, 0),
		UIKeyCommand.inputRightArrow: KeyboardMapEntry(39, 0, 0),
		UIKeyCommand.inputDownArrow: KeyboardMapEntry(40, 0, 0)
	]

	@objc
	func keyCMDhandle(_ command: UIKeyCommand) {
		let shiftKey = command.modifierFlags.contains(.shift)
		let ctrlKey = command.modifierFlags.contains(.control)
		let altKey = command.modifierFlags.contains(.alternate)
		let cmdKey = command.modifierFlags.contains(.command)

		var keycode = 0
		var keypressKeycode = 0
		let keyAction: String?

		if let input = command.input,
			let entry = DSTab.mapKeyboard[input] {

			keycode = entry.keycode
			keypressKeycode = shiftKey ? entry.shiftKeycode : entry.keypressKeycode
		}

		if keycode < 1 {
			return print("[Tab \(index)] unknown hardware keyboard input: \"\(command.input ?? "")\"")
		}

		switch command.input {
		case " ":
			keyAction = "__endless.smoothScroll(0, window.innerHeight * 0.75, 0, 0);"

		case UIKeyCommand.inputLeftArrow:
			keyAction = "__endless.smoothScroll(-75, 0, 0, 0);"

		case UIKeyCommand.inputRightArrow:
			keyAction = "__endless.smoothScroll(75, 0, 0, 0);"

		case UIKeyCommand.inputUpArrow:
			keyAction = cmdKey
				? "__endless.smoothScroll(0, 0, 1, 0);"
				: "__endless.smoothScroll(0, -75, 0, 0);"

		case UIKeyCommand.inputDownArrow:
			keyAction = cmdKey
				? "__endless.smoothScroll(0, 0, 0, 1);"
				: "__endless.smoothScroll(0, 75, 0, 0);"

		default:
			keyAction = nil
		}

		let js = String(format: "__endless.injectKey(%d, %d, %@, %@, %@, %@, %@);",
						keycode,
						keypressKeycode,
						(ctrlKey ? "true" : "false"),
						(altKey ? "true" : "false"),
						(shiftKey ? "true" : "false"),
						(cmdKey ? "true" : "false"),
						(keyAction != nil ? "function() { \(keyAction!) }" : "null"))

		print("[Tab \(index)] hardware keyboard input: \"\(command.input ?? "")\", keycode=\(keycode), keypressKeycode=\(keypressKeycode), modifierFlags=\(command.modifierFlags): shiftKey=\(shiftKey), ctrlKey=\(ctrlKey), altKey=\(altKey), cmdKey=\(cmdKey)")
		print("[Tab \(index)] injected JS: \(js)")

		stringByTestingJS(from: js)
	}
}
