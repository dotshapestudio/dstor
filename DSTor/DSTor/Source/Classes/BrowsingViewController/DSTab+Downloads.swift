//
//  Tab+Downloads.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 22.11.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import Foundation
import QuickLook

extension DSTab: DownloadTaskDelegate, QLPreviewControllerDelegate, QLPreviewControllerDataSource {

	/**
	Should be called whenever navigation occurs or
	when the WebViewTab is being closed.
	*/
	func dismissDownload() {
		if fileDownloaded != nil {
			// Delete the temporary file.
			try? FileManager.default.removeItem(atPath: fileDownloaded!.path)

			fileDownloaded = nil
		}

		if qlPrevController != nil {
			qlPrevController?.view.removeFromSuperview()
			qlPrevController?.removeFromParent()
			qlPrevController = nil
		}

		cover.removeFromSuperview()
	}


	// MARK: DownloadTaskDelegate

	func didStartDownloadingFile() {
		// Nothing to do here.
	}

	func didFinishDownloading(to location: URL?) {
		DispatchQueue.main.async {
			if location != nil {
				self.fileDownloaded = location;
				self.showDownload()
			}
		}
	}

	func setProgress(_ pr: NSNumber?) {
		progress = pr?.floatValue ?? 0
	}


	// MARK: QLPreviewControllerDelegate

	func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
		return fileDownloaded! as NSURL
	}


	// MARK: QLPreviewControllerDataSource

	func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
		return fileDownloaded != nil ? 1 : 0
	}

	// MARK: Private Methods

	private func showDownload() {
		qlPrevController = QLPreviewController()
		qlPrevController?.delegate = self
		qlPrevController?.dataSource = self

		AppDelegate.shared?.browsingUi?.addChild(qlPrevController!)

		qlPrevController?.view.add(to: self)

		qlPrevController?.didMove(toParent: AppDelegate.shared?.browsingUi)

		// Positively show toolbar, as users can't scroll it back up.
		scroll.delegate?.scrollViewDidScrollToTop?(scroll)
	}
}
