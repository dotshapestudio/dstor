//
//  BrowsingViewController+Tabs.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 31.10.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import Foundation

extension DSMainWebController: UICollectionViewDataSource, UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout, UICollectionViewDragDelegate,
UICollectionViewDropDelegate, UITableViewTabCellDelegate {

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return collectionViewForTabs != nil && collectionViewForTabs.isHidden ? .default : .lightContent
	}

	@objc func showTabsScreen() {
		resignFRToSearchField()

		self.collectionViewForTabs.reloadData()

		view.backgroundColor = .darkGray

		UIView.animate(withDuration: 0.5, animations: {
			self.view.setNeedsLayout()
		}) { _ in
			self.setNeedsStatusBarAppearanceUpdate()
		}

        var itemNumber: Int = 0
        
        if let currentTab = usedTab {
            itemNumber = self.tabList.firstIndex(of: currentTab) ?? 0
        }
        
		view.transition({
			self.topSearchView.isHidden = true
			self.progBar.isHidden = true
			self.container.isHidden = true
            self.bottomView.isHidden = true
			self.collectionViewForTabs.isHidden = false
            self.headerViewForAddTab.isHidden =  false
            self.collectionViewForTabs.scrollToItem(at: IndexPath(item: itemNumber, section: 0), at: .centeredHorizontally, animated: false)
		})
	}

	@objc func newTabFromTabsScreen() {
		insertTab(transition: .notAnimated) { _ in
			self.hideTabsScreen()
		}
	}

	@objc func hideTabsScreen() {
		hideTabsScreen(completion: nil)
	}


	// MARK: UICollectionViewDataSource

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return collectionViewForTabs.isHidden ? 0 : tabList.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)

		if let cell = cell as? TabCell {

			// Crash reports show, that indexPath.row can sometimes be different then
			// the real size of `tabs`. No idea why that race condition happens, but
			// we should at least not crash, if so.
			if indexPath.row > -1 && indexPath.row < tabList.count {
				let tab = tabList[indexPath.row]

				cell.name.text = tab.name

				tab.add(to: cell.tabContainer)
				tab.isHidden = false
				tab.isUserInteractionEnabled = false
			}

			cell.delegate = self
		}

		return cell
	}


	// MARK: UICollectionViewDelegate

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if let cell = collectionView.cellForItem(at: indexPath) as? TabCell {
			usedTab = tabList.first { $0 == cell.tabContainer.subviews.last }
			hideTabsScreen(completion: nil)
		}
	}

	// MARK: UICollectionViewDelegateFlowLayout

	func collectionView(_ collectionView: UICollectionView, layout
		collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

		let size = UIScreen.main.bounds.size

		// Could be 0 or less, so secure against becoming negative with minimum width of smallest iOS device.
        var width = size.width - (2 * 25) // left right 8
        var height = size.height - (2 * 8) // top bottom 8
        
        width = width / 1.1
        height = height / 1.4
        

        if tabList.count == 1 {
            let margin: CGFloat = (size.width - width) / 2
            let flow: UICollectionViewFlowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            flow.sectionInset = UIEdgeInsets(top: 8, left: margin, bottom: 8, right: margin)
        } else {
            let flow: UICollectionViewFlowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            flow.sectionInset = UIEdgeInsets(top: 8, left: 25, bottom: 8, right: 25)
        }
        
        return CGSize(width: width, height: height)
	}


	// MARK: UICollectionViewDragDelegate

	func collectionView(_ collectionView: UICollectionView, itemsForBeginning
		session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {

		let tab = tabList[indexPath.row]

		let dragItem = UIDragItem(itemProvider: NSItemProvider(object: tab.link.absoluteString as NSString))
		dragItem.localObject = tab

		return [dragItem]
	}


	// MARK: UICollectionViewDropDelegate

	func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate
		session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?)
		-> UICollectionViewDropProposal {

			if collectionViewForTabs.hasActiveDrag {
				return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
			}

			return UICollectionViewDropProposal(operation: .forbidden)
	}

	func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
		if coordinator.proposal.operation == .move,
			let item = coordinator.items.first,
			let source = item.sourceIndexPath,
			let destination = coordinator.destinationIndexPath {

			collectionView.performBatchUpdates({
				let tab = tabList.remove(at: source.row)
				tabList.insert(tab, at: destination.row)
				usedTab = tab

				collectionView.deleteItems(at: [source])
				collectionView.insertItems(at: [destination])
			})

			coordinator.drop(item.dragItem, toItemAt: destination)
		}
	}


	// MARK: UITableViewTabCellDelegate

	func dismiss(_ sender: TabCell) {
		if let indexPath = collectionViewForTabs.indexPath(for: sender), indexPath.row > -1 {

			// Crash reports show, that indexPath.row can sometimes be different then
			// the real size of `tabs`. No idea why that race condition happens, but
			// we should at least not crash, if so.
			if indexPath.row < tabList.count {
				deleteTab(tabList[indexPath.row])
			}

			collectionViewForTabs.performBatchUpdates({
				collectionViewForTabs.deleteItems(at: [indexPath])
			})
		}
	}


	// MARK: Private Methods

	private func hideTabsScreen(completion: ((_ finished: Bool) -> Void)?) {

		// UIViews can only ever have one superview. Move back from tabsCollection to container now.
		for tab in tabList {
			tab.isHidden = tab != usedTab
			tab.isUserInteractionEnabled = true
			tab.add(to: container)
		}

		updateProgress()

		if #available(iOS 13.0, *) {
			self.view.backgroundColor = .systemBackground
		}
		else {
			self.view.backgroundColor = .white
		}

		UIView.animate(withDuration: 0.5, animations: {
			self.view.setNeedsLayout()
		}) { _ in
			self.setNeedsStatusBarAppearanceUpdate()
		}

		view.transition({
			self.topSearchView.isHidden = false
			self.collectionViewForTabs.isHidden = true
			self.container.isHidden = false
            self.headerViewForAddTab.isHidden =  true
            self.bottomView.isHidden = false
//			self.tabsTools.isHidden = true
//			self.mainTools?.isHidden = false
		}, completion)
	}
}
