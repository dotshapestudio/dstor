//
//  BrowsingViewController.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 29.10.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

class DSMainWebController: UIViewController, DSTabProtocol {

    
    // MARK: - Properties
    
	@objc
	enum Animation: Int {
		case `default`
		case notAnimated
		case inBackground
	}

    static var shared:DSMainWebController!
    
    let btnSettings: UIButton = UIButton()
    
    let headerViewForAddTab: UIView = UIView()
    var btnToMoveForward: UIButton!
    var btnToMoveBackward: UIButton!
    var btnToViewCircuits: UIButton!
	let topSearchView: UIView = UIView()
    var btnShowBookmark: UIButton!

	var searchFl: UITextField! {
		didSet {
            searchFl.rightView = viewModel.reloadBt
		}
	}
    let bottomView: UIView = UIView()
    
    let progBar: UIProgressView = UIProgressView(progressViewStyle: .default)
	let container: UIView = UIView()

	var collectionViewForTabs: UICollectionView! {
		didSet {
			collectionViewForTabs.dragInteractionEnabled = true
		}
	}
    
    var btnTabsOption: UIButton!

	@objc
	var tabList = [DSTab]()

	private var currentIndexForTab = -1

	@objc
    var usedTab: DSTab? {
		get {
			return currentIndexForTab < 0 || currentIndexForTab >= tabList.count ? tabList.last : tabList[currentIndexForTab]
		}
		set {
			if let tab = newValue {
				currentIndexForTab = tabList.firstIndex(of: tab) ?? -1
			}
			else {
				currentIndexForTab = -1
			}

			if usedTab?.shouldReload ?? false {
				usedTab?.reload()
			}
		}
	}

	lazy var browsingHistoryVC = BrowsingHistoryVC()
    
    var viewModel = DSBrowsingViewModel()
    
    
    // MARK: - View Controller Life Cycle

	override func viewDidLoad() {
		super.viewDidLoad()
        
        weak var weakSelf: DSMainWebController? = self
        DispatchQueue.main.async {
            weakSelf?.generateUI()
            weakSelf?.updateProgress()
        }
        
		// There could have been tabs added before XIB was initialized.
		for tab in tabList {
			tab.add(to: container)
		}

		
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        registerObserver()
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

		// We made it this far, remove lock on previous startup.
		Configrations.torStateRegenerateLock = false
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        let nc = NotificationCenter.default

        nc.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        nc.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}

	override func encodeRestorableState(with coder: NSCoder) {
		super.encodeRestorableState(with: coder)

		var tabInfo = [[String: Any]]()

		for tab in tabList {
			tabInfo.append(["url": tab.link])

			// TODO: From old code. Why here this side effect?
			// Looks strange.
			tab.restorationIdentifier = tab.link.absoluteString
		}

		coder.encode(tabInfo, forKey: "webViewTabs")
		coder.encode(NSNumber(value: currentIndexForTab), forKey: "curTabIndex")
	}

	override func decodeRestorableState(with coder: NSCoder) {
		super.decodeRestorableState(with: coder)

		let tabInfo = coder.decodeObject(forKey: "webViewTabs") as? [[String: Any]]

		for info in tabInfo ?? [] {
			debug("Try restoring tab with \(info).")

			if let url = info["url"] as? URL {
				insertTab(url, forRestoration: true, transition: .notAnimated)
			}
		}

		if let index = coder.decodeObject(forKey: "curTabIndex") as? NSNumber {
			currentIndexForTab = index.intValue
		}

		for tab in tabList {
			tab.isHidden = tab != usedTab
			tab.isUserInteractionEnabled = true
			tab.add(to: container)
		}

		usedTab?.reload()

		updateProgress()
	}
    
    // MARK: - PRIVATE METHODS
    
    func registerObserver() {
        let nc = NotificationCenter.default

        nc.addObserver(self,
                       selector: #selector(willAppearKeyboard(notification:)),
                       name: UIResponder.keyboardWillShowNotification,
                       object: nil)

        nc.addObserver(self, selector: #selector(willDisappearKeyboard(notification:)),
                       name: UIResponder.keyboardWillHideNotification,
                       object: nil)
    }
    
    @objc func willAppearKeyboard(notification: Notification) {
        if let kbSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
            browsingHistoryVC.tableView.contentInset = insets
            browsingHistoryVC.tableView.scrollIndicatorInsets = insets
        }
    }

    @objc func willDisappearKeyboard(notification: Notification) {
        browsingHistoryVC.tableView.contentInset = .zero
        browsingHistoryVC.tableView.scrollIndicatorInsets = .zero
    }
    
    


	// MARK: DSTabProtocol

    func updateProgress() {
        debug("#updateChrome progress=\(usedTab?.progress ?? 1)")
        
        //		if let progress = progress {
        self.progBar.progress = usedTab?.progress ?? 1
        
        
        
        
        if self.progBar.progress >= 1 {
            if !progBar.isHidden {
                view.transition({
                    self.progBar.isHidden = true
                })
            }
        }
        else {
            if self.progBar.isHidden {
                view.transition({
                    self.progBar.isHidden = false
                    
                })
            }
        }
        //		}
        updateSrarchBar()
        btnReloadUpdate()
        forAndBackBtnUpdate()
    }

	@objc
	@discardableResult
	func insertTab(_ url: URL? = nil) -> DSTab? {
		return insertTab(url, forRestoration: false)
	}

	@discardableResult
	func insertTab(_ url: URL? = nil, forRestoration: Bool = false,
				   transition: Animation = .default, completion: ((Bool) -> Void)? = nil) -> DSTab? {

		debug("#insertTab url=\(String(describing: url)), forRestoration=\(forRestoration), transition=\(transition), completion=\(String(describing: completion))")

		let tab = DSTab(restorationId: forRestoration ? url?.absoluteString : nil)

		if !forRestoration {
			tab.load(url)
		}

		tab.delegete = self

		tabList.append(tab)

		tab.scroll.delegate = self
		tab.isHidden = true
		tab.add(to: container)

		let animations = {
			for otherTab in self.tabList {
				otherTab.isHidden = otherTab != tab
			}
		}

		let completionForeground = { (finished: Bool) in
			self.usedTab = tab

			self.updateProgress()

			completion?(finished)
		}

		switch transition {
		case .notAnimated:
			animations()
			completionForeground(true)

		case .inBackground:
			completion?(true)

		default:
			container.transition(animations, completionForeground)
		}

		return tab
	}

	func deleteTab(_ tab: DSTab, focus: DSTab? = nil) {
		debug("#removeTab tab=\(tab) focus=\(String(describing: focus))")

		resignFRToSearchField()

		container.transition({
			tab.isHidden = true
			(focus ?? self.tabList.last)?.isHidden = false
		}) { _ in
			self.usedTab = focus ?? self.tabList.last

			let hash = tab.hash

			tab.dismiss()
			self.tabList.removeAll { $0 == tab }

			AppDelegate.shared?.cookieJar.clearNonWhitelistedData(forTab: UInt(hash))

			self.updateProgress()
		}
	}

	func tab(ipcId: String?) -> DSTab? {
		return tabList.first { $0.idIPC == ipcId }
	}

	func tab(hash: Int?) -> DSTab? {
		return tabList.first { $0.hash == hash }
	}

	func index(of tab: DSTab) -> Int? {
		return tabList.firstIndex(of: tab)
	}

	func resignFRToSearchField() {
		if searchFl.isFirstResponder {
			searchFl.resignFirstResponder()
		}
	}


	// MARK: Public Methods

	@objc
	func shouldShow() {
		if tabList.count < 1 || Configrations.startWithNewTab {
			insertTab()
			Configrations.startWithNewTab = false
		}
	}

	@objc
	func addTab() {
		insertTab() { _ in
			self.makeSearchFieldFR()
		}
	}

	@objc
	func moveToTab(_ index: Int) {
		if index < 0 || index >= tabList.count {
			return
		}

		let focussing = tabList[index]

		container.transition({
			for tab in self.tabList {
				tab.isHidden = tab != focussing
			}
		}) { _ in
			self.usedTab = focussing
			self.updateProgress()
		}
	}

	@objc
	func deleteUsedTab() {
		guard let currentTab = usedTab else {
			return
		}

		deleteTab(currentTab)
	}

	@objc
	func deleteAllTabs() {
		for tab in tabList {
			tab.dismiss()
		}

		tabList.removeAll()

		usedTab = nil

		AppDelegate.shared?.cookieJar.clearAllNonWhitelistedData()

		self.updateProgress()
	}

	@objc
	func makeSearchFieldFR() {
		if !searchFl.isFirstResponder {
			searchFl.becomeFirstResponder()
		}
	}

	func debug(_ msg: String) {
		print("[\(String(describing: type(of: self)))] \(msg)")
	}
    
    /**
    Shows either a reload or stop icon, depending on if the current tab is currently loading or not.
    */
    private func btnReloadUpdate() {
        if usedTab?.isWaiting ?? false {
            viewModel.reloadBt.setImage(DSBrowsingViewModel.stopImg, for: .normal)
            viewModel.reloadBt.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
        }
        else {
            viewModel.reloadBt.setImage(DSBrowsingViewModel.reloadImg, for: .normal)
            viewModel.reloadBt.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    private func forAndBackBtnUpdate() {
        if usedTab?.browserCanGoBack ?? false {
            btnToMoveBackward.tintColor = .getColor(.DSPurple)
            btnToMoveBackward.isUserInteractionEnabled = true
        } else {
            btnToMoveBackward.tintColor = .lightGray
            btnToMoveBackward.isUserInteractionEnabled = false
        }
        
        if usedTab?.browserCanGoForward ?? false {
            btnToMoveForward.tintColor = .getColor(.DSPurple)
            btnToMoveForward.isUserInteractionEnabled = true
        } else {
            btnToMoveForward.tintColor = .lightGray
            btnToMoveForward.isUserInteractionEnabled = false
        }
    }

    @IBAction func buttonsListener(_ sender: UIButton) {
        resignFRToSearchField()
        switch sender {
        case viewModel.reloadBt:
            if usedTab?.isWaiting ?? false {
                usedTab?.hold()
            }
            else {
                usedTab?.reload()
            }
            btnReloadUpdate()
        case btnToMoveBackward:
            usedTab?.browserGoPrev()

        case btnToMoveForward:
            usedTab?.browserGoNext()
        case btnShowBookmark:
            let vc = DSBookmarkListViewController()
            self.present(vc, animated: true, completion: nil)
        case btnTabsOption:
            showTabsScreen()
        case btnSettings:
//            let storyboard = UIStoryboard(storyboard: .DSSetting)
            let vc = DSSettingListViewController()//storyboard.instantiateViewController(identifier: DSSettingListViewController.identifier)
            self.present(vc, animated: true, completion: nil)
        case btnToViewCircuits:
            let vc: DSCircuitsVC = DSCircuitsVC()
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.sourceView = sender.superview
            vc.popoverPresentationController?.sourceRect = sender.frame
            vc.modalTransitionStyle = .crossDissolve
            vc.popoverPresentationController?.delegate = vc
            
            //MARK: ViewModel Initialization
            vc.viewModel = DSCircuitsViewModel(url: usedTab?.link.beautify)
            self.present(vc, animated: true)
        default:
            break
        }
    }
}

