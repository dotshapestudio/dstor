//
//  TabCell.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 06.11.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

protocol UITableViewTabCellDelegate: class {
    func dismiss(_ sender: TabCell)
}

class TabCell: UICollectionViewCell {
    
    let name: UILabel = UILabel()
    let tabContainer: UIView = UIView()

    weak var delegate: UITableViewTabCellDelegate?

	override init(frame: CGRect) {
		super.init(frame: frame)

		create()
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)

		create()
	}
    
    


	// MARK: Actions

    @IBAction func dismiss() {
        delegate?.dismiss(self)
    }

	// MARK: Private Methods

	private func create() {
        
        
        DispatchQueue.main.async { [weak self] in
            self?.initUI()
            
            self?.layer.shadowColor = UIColor.black.cgColor
            self?.layer.shadowOffset = CGSize(width: -1, height: 1)
            self?.layer.shadowOpacity = 0.5
            self?.layer.shadowRadius = 8
            self?.layer.masksToBounds = false
        }
	}
}

// MARK: - UI

extension TabCell {
    private func initUI() {
        titleUI()
        containerUI()
        closeBtUI()
    }
    
    private func titleUI() {
        name.translatesAutoresizingMaskIntoConstraints = false
        name.textColor = .white
        name.font = UIFont.boldSystemFont(ofSize: 15)
        
        self.contentView.addSubview(name)
        NSLayoutConstraint.activate([
            
            name.topAnchor.constraint(equalTo: self.topAnchor),
            name.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            name.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            name.heightAnchor.constraint(equalToConstant: 24)
        ])
    }
    
    private func containerUI() {
        tabContainer.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(tabContainer)
        
        NSLayoutConstraint.activate([
            tabContainer.topAnchor.constraint(equalTo: name.bottomAnchor),
            tabContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tabContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
    }
    
    private func closeBtUI() {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.black
        button.setImage(UIImage.init(systemName: "xmark"), for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        self.contentView.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 30),
            button.widthAnchor.constraint(equalToConstant: 30),
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            button.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            button.topAnchor.constraint(equalTo: tabContainer.bottomAnchor, constant: 20)
        ])
    }
}


