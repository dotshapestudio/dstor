//
//  BrowsingViewController+UITextFieldDelegate.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 06.11.19.
//  Copyright © 2019 jcs. All rights reserved.
//

import Foundation

extension DSMainWebController: UITextFieldDelegate {

	private static let protectedImage = UIImage(named: "secure")
	private static let unprotectedImage = UIImage(named: "insecure")


	// MARK: UITextFieldDelegate

	func textFieldDidBeginEditing(_ textField: UITextField) {
		debug("#textFieldDidBeginEditing")

		updateSrarchBar()
	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		let search = searchFl.text

		DispatchQueue.main.async {
			self.browsingHistoryVC.hideSearchController()
			textField.resignFirstResponder()

			// User is shifting to a new place. Probably a good time to clear old data.
			AppDelegate.shared?.cookieJar.clearAllNonWhitelistedData()

			if let url = self.searchParser(search) {
				self.debug("#textFieldShouldReturn url=\(url)")

				if let currentTab = self.usedTab {
					currentTab.load(url)
				}
				else {
					self.insertTab(url)
				}
			}
			else {
				self.debug("#textFieldShouldReturn search=\(String(describing: search))")

				if self.usedTab == nil {
					self.insertTab()
				}

				self.usedTab?.find(for: search)
			}
		}

		return true
	}

	func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
		debug("#textFieldDidEndEditing")

		browsingHistoryVC.hideSearchController()
		updateSrarchBar()
	}


	// MARK: Actions

	@IBAction func textFieldTextChanged() {
		guard Configrations.findLive else {
			return
		}

		if searchParser(searchFl.text) != nil {
			// That's not a search, that's a valid URL. -> Remove live search results.

			return browsingHistoryVC.hideSearchController()
		}

		if !browsingHistoryVC.isSearching {
			if UIDevice.current.userInterfaceIdiom == .pad {
				present(browsingHistoryVC, searchFl)
			}
			else {
				addChild(browsingHistoryVC)

				browsingHistoryVC.view.translatesAutoresizingMaskIntoConstraints = false
				view.addSubview(browsingHistoryVC.view)
				browsingHistoryVC.view.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
				browsingHistoryVC.view.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
				browsingHistoryVC.view.topAnchor.constraint(equalTo: topSearchView.bottomAnchor).isActive = true
				browsingHistoryVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
			}

			browsingHistoryVC.isSearching = true
		}

		browsingHistoryVC.updateQuery(searchFl.text, tab: usedTab)
	}


	// MARK: Public Methods

	/**
	Renders the `searchFl` depending on if it currently has focus.
	*/
	func updateSrarchBar() {
		if searchFl.isFirstResponder {
			if searchFl.textAlignment == .natural {
				// Seems already set correctly. Don't mess with it, while user
				// edits it actively!
				return
			}

			searchFl.text = usedTab?.link.beautify?.absoluteString

			// .unlessEditing would be such a great state, if it wouldn't show
			// while editing an empty field. Argh.
			searchFl.leftViewMode = .never
			searchFl.rightViewMode = .never

			searchFl.textAlignment = .natural
		}
		else {
			searchFl.text = DSMainWebController.prettyTitle(usedTab?.link)
//			searchFl.leftViewMode = encryptionBt.image(for: .normal) == nil ? .never : .always
			searchFl.rightViewMode = searchFl.text?.isEmpty ?? true ? .never : .always

			searchFl.textAlignment = .center
		}
	}

	class func prettyTitle(_ url: URL?) -> String {
		guard let url = url?.beautify else {
			return ""
		}

		if let host = url.host {
			return host.replacingOccurrences(of: #"^www\d*\."#, with: "", options: .regularExpression)
		}

		return url.absoluteString
	}


	// MARK: Private Methods

	/**
	Parse a user search.

	- parameter search: The user entry, which could be a (semi-)valid URL or a search engine query.
	- returns: A parsed (and fixed) URL or `nil`, in which case you should treat the string as a search engine query.
	*/
	private func searchParser(_ search: String?) -> URL? {
		// Must not be empty, must not be the explicit blank page.
		if let search = search,
			!search.isEmpty {

			// blank page, return that.
			if search.caseInsensitiveCompare(URL.empty.absoluteString) == .orderedSame {
				return URL.empty
			}

			// If credits page, return that.
			if search.caseInsensitiveCompare(URL.aboutDSTor.absoluteString) == .orderedSame {
				return URL.aboutDSTor
			}

			if search.caseInsensitiveCompare(URL.securityLevels_page.absoluteString) == .orderedSame {
				return URL.securityLevels_page
			}

			if search.range(of: #"\s+"#, options: .regularExpression) != nil
				|| !search.contains(".") {
				// Search contains spaces or contains no dots. That's really a search!
				return nil
			}

			// We rely on URLComponents parsing style! *Don't* change to URL!
			if let urlc = URLComponents(string: search) {
				let scheme = urlc.scheme?.lowercased() ?? ""

				if scheme.isEmpty {
					// Set missing scheme to HTTP.
					return URL(string: "http://\(search)")
				}

				if scheme != "about" && scheme != "file" {
					if urlc.host?.isEmpty ?? true
						&& urlc.path.range(of: #"^\d+"#, options: .regularExpression) != nil {

						// A scheme, no host, path begins with numbers. Seems like "example.com:1234" was parsed wrongly.
						return URL(string: "http://\(search)")
					}

					// User has simply entered a valid URL?!?
					return urlc.url
				}

				// Someone wants to try something here. No way.
			}

			// Unparsable.
		}

		//  Return start page.
		return URL.start
	}
}
