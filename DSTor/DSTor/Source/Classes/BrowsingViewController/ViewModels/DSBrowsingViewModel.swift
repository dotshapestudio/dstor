//
//  DSBrowsingViewModel.swift
//  DSTor
//
//  Created by Shahbaz Akram on 27/04/2021.
//

import Foundation

class DSBrowsingViewModel {
    static let reloadImg = UIImage(systemName: "goforward")
    static let stopImg = UIImage(named: "close")
    
    
    lazy var reloadBt: UIButton = {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.setImage(DSBrowsingViewModel.reloadImg, for: .normal)

        if #available(iOS 13, *) {
            button.tintColor = .label
        }
        else {
            button.tintColor = .black
        }

        button.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true

        button.addTarget(DSMainWebController.shared, action: #selector(DSMainWebController.buttonsListener), for: .touchUpInside)

        return button
    }()
}

