//
//  DSCircuitsViewModel.swift
//  DSTor
//
//  Created by Macbook on 05/07/2021.
//

class DSCircuitsViewModel: NSObject {
    
    struct Node {
        var title: String
        var ip: String?
        var note: String?

        init(title: String, ip: String? = nil, note: String? = nil) {
            self.title = title
            self.ip = ip
            self.note = note
        }

        init(_ torNode: TorNode) {
            self.title = torNode.localizedCountryName ?? torNode.countryCode ?? torNode.nickName ?? ""
            self.ip = torNode.ipv4Address?.isEmpty ?? true ? torNode.ipv6Address : torNode.ipv4Address
        }
    }

    public var currentUrl: URL?
    
    var nodes = [Node]()
    private var usedCircuits = [TorCircuit]()

    private static let onionAddressRegex = try? NSRegularExpression(pattern: "^(.*).onion$", options: .caseInsensitive)

    private static let beginningOfTime = Date(timeIntervalSince1970: 0)
    
    init(url: URL?) {
        currentUrl = url
    }
    
    // MARK: Utility Methods
    
    func newCircuitSetup(dismiss: @escaping (() -> Void)) {
        DSTorManager.shared.closeTorCircuits(usedCircuits) { _ in
            AppDelegate.shared?.browsingUi?.usedTab?.reload()
            dismiss()
        }
    }

    func reloadCircuits(reload: @escaping (() -> Void)) {
        DispatchQueue.global(qos: .userInitiated).async {
            DSTorManager.shared.getTorCircuits { circuits in

                // Store in-use circuits (identified by having a SOCKS username,
                // so the user can close them and get fresh ones on #newCircuits.
                self.usedCircuits = circuits.filter { !($0.socksUsername?.isEmpty ?? true) }

                var candidates = [TorCircuit]()
                var query: String?

                if let host = self.currentUrl?.host {
                    let matches = Self.onionAddressRegex?.matches(
                        in: host, options: [],
                        range: NSRange(host.startIndex ..< host.endIndex, in: host))

                    if matches?.first?.numberOfRanges ?? 0 > 1,
                        let nsRange = matches?.first?.range(at: 1),
                        let range = Range(nsRange, in: host) {
                        query = String(host[range])
                    }
                }

                // Circuits used for .onion addresses can be identified by their
                // rendQuery, which is equal to the "domain".
                if let query = query {
                    for circuit in circuits {
                        if circuit.purpose == TorCircuit.purposeHsClientRend
                            && circuit.rendQuery == query {

                            candidates.append(circuit)
                        }
                    }
                }
                else {
                    for circuit in circuits {
                        if circuit.purpose == TorCircuit.purposeGeneral
                            && !(circuit.socksUsername?.isEmpty ?? true)
                            && !(circuit.buildFlags?.contains(TorCircuit.buildFlagIsInternal) ?? false)
                            && !(circuit.buildFlags?.contains(TorCircuit.buildFlagOneHopTunnel) ?? false) {

                            candidates.append(circuit)
                        }
                    }
                }

                // Oldest first! This is sometimes wrong, but our best guess.
                // Often times there are newer ones created after a request
                // but the main page was requested via the oldest one.
                candidates.sort { $0.timeCreated ?? Self.beginningOfTime
                    < $1.timeCreated ?? Self.beginningOfTime }

                self.nodes.removeAll()

                self.nodes.append(Node(title: NSLocalizedString("This browser", comment: "")))

                for node in candidates.first?.nodes ?? [] {
                    self.nodes.append(Node(node))
                }
                if self.nodes.count > 1 {
                    self.nodes[1].note = NSLocalizedString("Guard", comment: "")
                }

                self.nodes.append(Node(title: DSMainWebController.prettyTitle(self.currentUrl)))

                reload()
            }
        }
    }
    
}

