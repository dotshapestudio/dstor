//
//  DSCircuitsVC.swift
//  DSTor
//
//  Created by Macbook on 05/07/2021.
//

import UIKit

fileprivate enum CircuitCellType: Int {
    case topCell
    case centerCell
    case bottomCell
}

class DSCircuitsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate {

    private let tableView: UITableView = UITableView()
    private let theme_color: UIColor = .getColor(.DSPurple)
    var viewModel: DSCircuitsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        weak var weakSelf: DSCircuitsVC! = self
        DispatchQueue.main.async {
            weakSelf.generateUI()
        }
    }
    
    override var preferredContentSize: CGSize {
        get {
            return CGSize(width: 300, height: 320)
        }
        
        set {
            // ignore
        }
    }

    //MARK: Generic Views
    private func getThemeColoredView() -> UIView {
        let sharedView: UIView = UIView()
        sharedView.translatesAutoresizingMaskIntoConstraints = false
        sharedView.backgroundColor = theme_color
        
        return sharedView
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    private func addTopView() -> UIView {
        let topView: UIView = getThemeColoredView()
        
        self.view.addSubview(topView)
        
        // MARK: TopView Constarints
        NSLayoutConstraint.activate([
            topView.leftAnchor.constraint(equalTo: view.leftAnchor),
            topView.rightAnchor.constraint(equalTo: view.rightAnchor),
            topView.topAnchor.constraint(equalTo: view.topAnchor),
            topView.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        addLblHeader(to: topView)
        
        return topView
    }
    
    private func addLblHeader(to subview: UIView) {
        let header: UILabel = UILabel()
        header.translatesAutoresizingMaskIntoConstraints = false
        header.textColor = .white
        header.font = UIFont.systemFont(ofSize: 14)
        header.text = "Tor Circuits"
        
        subview.addSubview(header)
        
        // MARK: TopView Constarints
        NSLayoutConstraint.activate([
            header.centerXAnchor.constraint(equalTo: subview.centerXAnchor),
            header.centerYAnchor.constraint(equalTo: subview.centerYAnchor)
        ])
    }
    
    private func addTableViewPLUSNewCircuitBtn(to superView: UIView) {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: .zero)
    
        superView.addSubview(tableView)
        
        // MARK: Button new circuit connection
        let actionBtn: UIButton = UIButton()
        actionBtn.translatesAutoresizingMaskIntoConstraints = false
        actionBtn.backgroundColor = theme_color
        actionBtn.titleLabel!.font = UIFont.boldSystemFont(ofSize: 14)
        actionBtn.setTitleColor(.white, for: .normal)
        actionBtn.setTitle("New Circuit for this Site", for: .normal)
        actionBtn.addTarget(self, action: #selector(newCircuitClicked(_:)), for: .touchUpInside)
        actionBtn.layer.cornerRadius = 15
        actionBtn.layer.masksToBounds = true
        
        superView.addSubview(actionBtn)
        
        // MARK: TableView Constarints
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: superView.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: superView.rightAnchor),
            tableView.topAnchor.constraint(equalTo: superView.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: actionBtn.topAnchor, constant: -8),
            actionBtn.bottomAnchor.constraint(equalTo: superView.bottomAnchor, constant: -8),
            actionBtn.centerXAnchor.constraint(equalTo: superView.centerXAnchor),
            actionBtn.heightAnchor.constraint(equalToConstant: 30),
            actionBtn.widthAnchor.constraint(equalTo: superView.widthAnchor, multiplier: 0.65)
        ])
    }
    
    private func setupBottomView() -> UIView {
        let bottomView: UIView = getThemeColoredView()
        
        self.view.addSubview(bottomView)
        
        // MARK: Constarints
        NSLayoutConstraint.activate([
            bottomView.leftAnchor.constraint(equalTo: view.leftAnchor),
            bottomView.rightAnchor.constraint(equalTo: view.rightAnchor),
            bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            bottomView.heightAnchor.constraint(equalToConstant: 53)
        ])
        
        addButton(to: bottomView)
        
        return bottomView
    }
    
    private func addButton(to bottomView: UIView)
    {
        let actionBtn: UIButton = UIButton()
        actionBtn.translatesAutoresizingMaskIntoConstraints = false
        actionBtn.backgroundColor = .clear
        actionBtn.titleLabel!.font = UIFont.systemFont(ofSize: 14)
        actionBtn.setTitleColor(.white, for: .normal)
        actionBtn.setTitle("Bridge Configration", for: .normal)
        actionBtn.addTarget(self, action: #selector(bridgeConfigrationClicked(_:)), for: .touchUpInside)
        
        bottomView.addSubview(actionBtn)
        
        // MARK: Constarints
        NSLayoutConstraint.activate([
            actionBtn.leftAnchor.constraint(equalTo: bottomView.leftAnchor),
            actionBtn.rightAnchor.constraint(equalTo: bottomView.rightAnchor),
            actionBtn.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor, constant: -13),
            actionBtn.topAnchor.constraint(equalTo: bottomView.topAnchor)
        ])
    }
    
    private func setupCenterViewConnected(topView: UIView, bottomView: UIView) -> UIView {
        let centerViewConnected: UIView = UIView()
        centerViewConnected.translatesAutoresizingMaskIntoConstraints = false
        centerViewConnected.backgroundColor = .clear
        
        self.view.addSubview(centerViewConnected)
        
        // MARK: Constarints
        NSLayoutConstraint.activate([
            centerViewConnected.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            centerViewConnected.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            centerViewConnected.bottomAnchor.constraint(equalTo: bottomView.topAnchor),
            centerViewConnected.topAnchor.constraint(equalTo: topView.bottomAnchor)
        ])
        
        addTableViewPLUSNewCircuitBtn(to: centerViewConnected)
        
        return centerViewConnected
    }
    
    private func setupCenterViewDisconnected(topView: UIView, bottomView: UIView) -> UIView {
        let centerViewDisconnected: UIView = UIView()
        centerViewDisconnected.translatesAutoresizingMaskIntoConstraints = false
        centerViewDisconnected.backgroundColor = .clear
        
        self.view.addSubview(centerViewDisconnected)
        
        // MARK: Constarints
        NSLayoutConstraint.activate([
            centerViewDisconnected.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            centerViewDisconnected.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            centerViewDisconnected.bottomAnchor.constraint(equalTo: bottomView.topAnchor),
            centerViewDisconnected.topAnchor.constraint(equalTo: topView.bottomAnchor)
        ])
        
        return centerViewDisconnected
    }
    
    //MARK: IBActions i.e btn clicks
    @IBAction func bridgeConfigrationClicked(_ sender: Any) {
        let bridgesCOntroller: DSBridgesVC = DSBridgesVC()
        bridgesCOntroller.modalPresentationStyle = .formSheet
        self.present(bridgesCOntroller, animated: true)
    }
    
    @IBAction func newCircuitClicked(_ sender: Any) {
        viewModel!.newCircuitSetup {
            DispatchQueue.main.async {
                self.dismiss(animated: true)
            }
        }
    }
    
    //MARK: UI generation
    private func generateUI() {
        self.view.backgroundColor = .white
        let topView = addTopView()
        let bottomView = setupBottomView()
        
        let connected: UIView = setupCenterViewConnected(topView: topView, bottomView: bottomView)
        let disconnected: UIView = setupCenterViewDisconnected(topView: topView, bottomView: bottomView)
        
        if viewModel!.currentUrl?.isUnique ?? true {
            connected.isHidden = true
            disconnected.isHidden = false
        } else {
            connected.isHidden = false
            disconnected.isHidden = true
            weak var weakSelf: DSCircuitsVC! = self
            viewModel!.reloadCircuits {
                DispatchQueue.main.async {
                    weakSelf.tableView.reloadData()
                }
            }
        }
    }
    
    //MARK: TableView Delegates and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.nodes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 37
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DSCircuitsTableViewCell = DSCircuitsTableViewCell()
        
        let data: DSCircuitsViewModel.Node = viewModel!.nodes[indexPath.row]
        var ipAdderess: String!
        var note: String!
        if let str = data.ip {
            ipAdderess = str
        } else {
            ipAdderess = ""
        }
        if let str = data.note {
            note = str
        } else {
            note = ""
        }
        
        if (indexPath.row == 0) {
            cell.generateUI(cellType: .topCell, country: data.title, IP: ipAdderess, IsGuard: note)
        } else if (indexPath.row == viewModel!.nodes.count - 1) {
            cell.generateUI(cellType: .bottomCell, country:"" , IP: data.title, IsGuard: note)
        } else {
            cell.generateUI(cellType: .centerCell, country: data.title, IP: ipAdderess, IsGuard: note)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    // MARK: UIPopoverPresentationControllerDelegate
    public func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: TableViewCell for DSCircuitsVC

fileprivate class DSCircuitsTableViewCell: UITableViewCell {
    
    private let theme_color: UIColor = .getColor(.DSPurple)
    private var country: String?
    private var IP: String?
    private var note: String?
    private var cellType: CircuitCellType?
    
    //MARK: Initializers
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    //MARK: Generate User Interface
    func generateUI(cellType type: CircuitCellType, country cn: String, IP ip: String, IsGuard guar: String) {
        country = cn
        IP = ip
        note = guar
        cellType = type
        
        weak var weakSelf: DSCircuitsTableViewCell? = self
        DispatchQueue.main.async {
            weakSelf?.setupView()
        }
    }
    
    //MARK: Generic Views
    private func getThemeColoredView() -> UIView {
        let sharedView: UIView = UIView()
        sharedView.translatesAutoresizingMaskIntoConstraints = false
        sharedView.backgroundColor = theme_color
        
        return sharedView
    }
    
    private func getLabel(text: String, withFont font: UIFont, andTextColor color: UIColor) -> UILabel {
        let sharedLabel: UILabel = UILabel()
        sharedLabel.translatesAutoresizingMaskIntoConstraints = false
        sharedLabel.font = font
        sharedLabel.text = text
        sharedLabel.textColor = color
        sharedLabel.textAlignment = .left
        
        return sharedLabel
    }
    
    private func setupView() {
        let circle: UIView = setupCircleView()
        
        let lineTop: UIView = getThemeColoredView()
        
        self.addSubview(lineTop)
        
        let lineBottom: UIView = getThemeColoredView()
        
        self.addSubview(lineBottom)
        
        //MARK: Constraint TopLine
        NSLayoutConstraint.activate([
            lineTop.widthAnchor.constraint(equalToConstant: 4),
            lineTop.centerXAnchor.constraint(equalTo: circle.centerXAnchor),
            lineTop.topAnchor.constraint(equalTo: self.topAnchor),
            lineTop.bottomAnchor.constraint(equalTo: circle.topAnchor)
        ])
        
        //MARK: Constraint BottomLine
        NSLayoutConstraint.activate([
            lineBottom.widthAnchor.constraint(equalToConstant: 4),
            lineBottom.centerXAnchor.constraint(equalTo: circle.centerXAnchor),
            lineBottom.topAnchor.constraint(equalTo: circle.bottomAnchor),
            lineBottom.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
        
        lineTop.isHidden = (cellType == .topCell)
        lineBottom.isHidden = (cellType == .bottomCell)
        
        let labelsView: UIView = getLabelsView()
        self.addSubview(labelsView)
        
        //MARK: Constraints
        NSLayoutConstraint.activate([
            labelsView.topAnchor.constraint(equalTo: self.topAnchor),
            labelsView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            labelsView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            labelsView.leftAnchor.constraint(equalTo: circle.rightAnchor, constant: 16),
        ])
    }
    
    private func setupCircleView() -> UIView {
        let circleView: UIView = getThemeColoredView()
        circleView.layer.cornerRadius = 7.5
        circleView.layer.masksToBounds = true
        
        self.addSubview(circleView)
        
        //MARK: Constraints
        NSLayoutConstraint.activate([
            circleView.widthAnchor.constraint(equalToConstant: 15),
            circleView.heightAnchor.constraint(equalToConstant: 15),
            circleView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            circleView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
        
        return circleView
    }
    
    private func getLabelsView() -> UIView {
        let outerView: UIView = UIView()
        outerView.translatesAutoresizingMaskIntoConstraints = false
        outerView.backgroundColor = .clear
        
        let left: UILabel = getLabel(text: country!, withFont: UIFont.systemFont(ofSize: 16), andTextColor: .black)
        let center: UILabel = getLabel(text: IP!, withFont: UIFont.systemFont(ofSize: 14), andTextColor: .lightGray)
        let right: UILabel = getLabel(text: note!, withFont: UIFont.boldSystemFont(ofSize: 16), andTextColor: .black)
        
        // MARK: Set Content Hugging priority of labels
        left.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        center.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        right.setContentHuggingPriority(.defaultLow, for: .horizontal)
        
        outerView.addSubview(left)
        outerView.addSubview(center)
        outerView.addSubview(right)
        
        // MARK: Relative to each other constraint
        NSLayoutConstraint.activate([
            left.leftAnchor.constraint(equalTo: outerView.leftAnchor),
            left.rightAnchor.constraint(equalTo: center.leftAnchor, constant: -4),
            center.rightAnchor.constraint(equalTo: right.leftAnchor, constant: -4),
            right.rightAnchor.constraint(equalTo: outerView.rightAnchor)
        ])
        
        // MARK: Vertical Center Constraint
        NSLayoutConstraint.activate([
            left.centerYAnchor.constraint(equalTo: outerView.centerYAnchor),
            center.centerYAnchor.constraint(equalTo: outerView.centerYAnchor),
            right.centerYAnchor.constraint(equalTo: outerView.centerYAnchor)
        ])
        
        return outerView
    }
}
