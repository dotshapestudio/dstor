//
//  DSContentPolicyVC.swift
//  DSTor
//
//  Created by Shahbaz Akram on 16/06/2021.
//

import UIKit

class DSContentPolicyVC: UIViewController {
    
    
    // MARK: - Properties
    var delegate: DSSettingListViewControllerDelegate?
    
    var viewModel = DSContentViewModel()
    let tableView:UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.tableFooterView = UIView()
        return table
    }()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        generateUI()

        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Public Methods
    class var identifier: String {
        return String(describing: self)
    }
}

extension DSContentPolicyVC:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        cell.tintColor = .getColor(.DSPurple)
        return viewModel.cellforRow(type: viewModel.contentType, indexPath: indexPath, cell: cell)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRow(indexPath: indexPath)
        delegate?.needsReload()
        tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Default"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
