//
//  DSContentViewModel.swift
//  DSTor
//
//  Created by Shahbaz Akram on 17/06/2021.
//

import Foundation


enum ContentType:Int {
    case userAgent = 0
    case searchEngine
    
    var description:String {
        switch self {
        case .userAgent:
            return "User Agent"
        case .searchEngine:
            return "Search Engine"
        }
    }
}

class DSContentViewModel {
    
    // Public Properties
    public var contentType:ContentType = .searchEngine
    public lazy var numberOfRows:Int = {
        return (contentType == .userAgent) ? DSSettingsViewModel.userAgents.count
            : (contentType == .searchEngine) ? Configrations.allSearchEnginesTtitles.count
            : 0
    }()
    
    // MARK: - Public Methods
    
    func cellforRow(type:ContentType, indexPath:IndexPath, cell:UITableViewCell) -> UITableViewCell{
        
        switch type {
        case .userAgent:
            cell.textLabel?.text = DSSettingsViewModel.userAgents[indexPath.row]
            if cell.textLabel?.text == HostConfigrations.byDefault().option_userAgent {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        case .searchEngine:
            cell.textLabel?.text = Configrations.allSearchEnginesTtitles[indexPath.row]
            if cell.textLabel?.text == Configrations.searchEngineTitle{
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    func didSelectRow(indexPath:IndexPath) {
        switch contentType {
        case .userAgent:
            let hostCOnfigs: HostConfigrations = HostConfigrations.byDefault()
            hostCOnfigs.option_userAgent = DSSettingsViewModel.userAgents[indexPath.row]
            hostCOnfigs.saveCopy().add()
        case .searchEngine:
            Configrations.searchEngineTitle = Configrations.allSearchEnginesTtitles[indexPath.row]
        }
    }
}
