//
//  DSContentPolicyVC+UI.swift
//  DSTor
//
//  Created by Shahbaz Akram on 27/07/2021.
//

import Foundation

extension DSContentPolicyVC {
    func generateUI() {
        view.backgroundColor = .white
        let top: UIView = DSNavigationView(title: viewModel.contentType.description, controller: self)
        loadTableView(navigationView: top)
    }
    
    func loadTableView(navigationView:UIView) {
        let guide = view.safeAreaLayoutGuide
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [tableView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)]
        )
        
    }
}
