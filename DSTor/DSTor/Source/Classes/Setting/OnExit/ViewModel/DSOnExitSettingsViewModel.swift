//
//  DSOnExitSettingsViewModel.swift
//  DSTor
//
//  Created by Faizan Siddiqui on 12/08/2021.
//


class DSOnExitSettingsViewModel {
    
    enum OnExitDeleteBtnType: Int {
        case cookies = 1112, cache = 1113, siteAndPlugInData = 1114
    }
    
    struct OnExitSettingsModel {
        let title: String
        let switchBtnType: OnExitDeleteBtnType
        
        init(with title: String, and switchType: OnExitDeleteBtnType) {
            self.title = title
            self.switchBtnType = switchType
        }
    }
    
    class var data: Array<OnExitSettingsModel> {
        var arr: Array<OnExitSettingsModel> = Array<OnExitSettingsModel>()
        
        arr.append(OnExitSettingsModel(with: "Delete Cookies", and: .cookies))
        arr.append(OnExitSettingsModel(with: "Delete Chache", and: .cache))
        arr.append(OnExitSettingsModel(with: "Delete othe Site and Plug-in Data", and: .siteAndPlugInData))
        
        return arr
    }
    
    class func setSwitchButtonSettings(with switchType: OnExitDeleteBtnType, isOn:Bool) {
        switch switchType {
        case .cache:
            OnExitActionVars.cache = isOn
        case .cookies:
            OnExitActionVars.cookies = isOn
        case .siteAndPlugInData:
            OnExitActionVars.siteAndPlugInData = isOn
        }
    }
        
    class func getSwicthValue(with switchType: OnExitDeleteBtnType) -> Bool {
        switch switchType {
        case .cache:
            return OnExitActionVars.cache
        case .cookies:
            return OnExitActionVars.cookies
        case .siteAndPlugInData:
            return OnExitActionVars.siteAndPlugInData
        }
    }
    
    class func eraseSettingsOnExit() {
        
        if OnExitActionVars.cookies {
            clearAllCookies()
        }
        
        let dataPaths: Array = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)
        if dataPaths.count > 0 {
            let dataDir: String = dataPaths.first!
            let fm: FileManager = FileManager.default
            if fm.fileExists(atPath: dataDir) {
                if OnExitActionVars.cookies {
                    let cookiesDir: String = "\(dataDir)/Cookies"
                    if fm.fileExists(atPath: cookiesDir) {
                        try? fm.removeItem(atPath: cookiesDir)
                    }
                }
                
                if OnExitActionVars.cache {
                    let cacheDir: String = "\(dataDir)/Caches"
                    if fm.fileExists(atPath: cacheDir) {
                        try? fm.removeItem(atPath: cacheDir)
                    }
                }
                
                if OnExitActionVars.siteAndPlugInData {
                    let siteAndPlugInDataDir: String = "\(dataDir)/WebKit"
                    if fm.fileExists(atPath: siteAndPlugInDataDir) {
                        try? fm.removeItem(atPath: siteAndPlugInDataDir)
                    }
                }
            }
        }
    }
    
    // MARK: Private Members and Functions
    
    private struct OnExitActionVars {
        static var cookies: Bool {
            get {
                return UserDefaults.standard.bool(forKey: "clear_\(OnExitDeleteBtnType.cookies.rawValue)")
            }
            
            set {
                UserDefaults.standard.setValue(newValue, forKey: "clear_\(OnExitDeleteBtnType.cookies.rawValue)")
            }
        }
        
        static var cache: Bool {
            get {
                return UserDefaults.standard.bool(forKey: "clear_\(OnExitDeleteBtnType.cache.rawValue)")
            }
            
            set {
                UserDefaults.standard.setValue(newValue, forKey: "clear_\(OnExitDeleteBtnType.cache.rawValue)")
            }
        }
        
        static var siteAndPlugInData: Bool {
            get {
                return UserDefaults.standard.bool(forKey: "clear_\(OnExitDeleteBtnType.siteAndPlugInData.rawValue)")
            }
            
            set {
                UserDefaults.standard.setValue(newValue, forKey: "clear_\(OnExitDeleteBtnType.siteAndPlugInData.rawValue)")
            }
        }
    }
    
    // MARK: App Terminating Functions
    private class func clearAllCookies() {
        cookieJar?.clearAllNonWhitelistedData()
        cookieData.removeAll()
    }
    
    private struct Item {
       let host: String

       var cookies = 0

       var storage: Int64 = 0

       init(_ host: String) {
           self.host = host
       }
    }
    
    private class var cookieJar: CookieJar? {
        return AppDelegate.shared?.cookieJar
    }
    
    private static var cookieData: [Item] = {
        var data = [String: Item]()

        if let cookies = cookieJar?.cookieStorage.cookies {
            for cookie in cookies {
                var host = cookie.domain

                if host.first == "." {
                    host.removeFirst()
                }

                var item = data[host] ?? Item(host)
                item.cookies += 1
                data[host] = item
            }
        }

        if let files = cookieJar?.localStorageFiles() {
            for item in files {
                if let filepath = item.key as? String,
                    let host = item.value as? String {

                    var item = data[host] ?? Item(host)
                    item.storage += (size(filepath) ?? 0)
                    data[host] = item
                }
            }
        }

        return data.map { $1 }.sorted { $0.storage == $1.storage ? $0.cookies > $1.cookies : $0.storage > $1.storage }
    }()
    
    /**
    Get size in byte of a given file.

    - parameter filepath: The path to the file.
    - returns: File size in bytes.
    */
    private class func size(_ filepath: String?) -> Int64? {
        if let filepath = filepath,
            let attr = try? FileManager.default.attributesOfItem(atPath: filepath) {
            return (attr[.size] as? NSNumber)?.int64Value
        }

        return nil
    }
}
