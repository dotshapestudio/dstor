//
//  DSOnExitCell.swift
//  DSTor
//
//  Created by Faizan Siddiqui on 12/08/2021.
//

import UIKit

class DSOnExitCell: UITableViewCell {
    
    private var titleLbl: UILabel!
    private var switchBt: UISwitch!

    class var rowHeight: CGFloat {
        return 50
    }
    
    class var headerHeight: CGFloat {
        return 30
    }
    
    // MARK: Initializers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        makeCell()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        makeCell()
    }
    
    func bindCell(with controller: DSOnExitSettingsVC, and index: Int) {
        
        let cellData: DSOnExitSettingsViewModel.OnExitSettingsModel = DSOnExitSettingsViewModel.data[index]
        
        titleLbl.text = cellData.title
        switchBt.tag = cellData.switchBtnType.rawValue
        switchBt.isOn = DSOnExitSettingsViewModel.getSwicthValue(with: cellData.switchBtnType)
        switchBt.addTarget(controller, action: #selector(controller.switchValueChanged(_:)), for: .valueChanged)
    }
    
    // MARK: UI Generation of Cell
    private func makeCell() {
        titleLbl = getLabel(font: .systemFont(ofSize: 16), alignment: .natural)
        switchBt = getSwitch()
        
        selectionStyle = .none
        titleLbl.numberOfLines = 2
        
        contentView.addSubview(titleLbl)
        contentView.addSubview(switchBt)
        
        NSLayoutConstraint.activate([
            titleLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleLbl.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            switchBt.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -30),
            switchBt.centerYAnchor.constraint(equalTo: titleLbl.centerYAnchor),
            titleLbl.trailingAnchor.constraint(equalTo: switchBt.leadingAnchor, constant: -8)
        ])
    }
    
    private func getLabel(font f: UIFont, alignment a: NSTextAlignment) -> UILabel {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = f
        label.textAlignment = a
        label.textColor = .black
        label.contentMode = .left
        
        return label
    }
    
    private func getSwitch() -> UISwitch {
        let btnSwitch: UISwitch = UISwitch()
        btnSwitch.translatesAutoresizingMaskIntoConstraints = false
        btnSwitch.isOn = true
        btnSwitch.onTintColor = UIColor.getColor(.DSPurple)
        btnSwitch.thumbTintColor = .white
        btnSwitch.contentHorizontalAlignment = .center
        btnSwitch.contentVerticalAlignment = .center
        btnSwitch.isEnabled = true
        
        return btnSwitch
    }
}
