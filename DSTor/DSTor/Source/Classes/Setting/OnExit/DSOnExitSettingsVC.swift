//
//  DSOnExitSettingsVC.swift
//  DSTor
//
//  Created by Faizan Siddiqui on 12/08/2021.
//

class DSOnExitSettingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let viewModel: DSOnExitSettingsViewModel = DSOnExitSettingsViewModel()
    
    override func loadView() {
        super.loadView()
        
        // UI Generation Code
        generateUI()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: UITableView Delegate and DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DSOnExitSettingsViewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DSOnExitCell = DSOnExitCell()
        cell.bindCell(with: self, and: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DSOnExitCell.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DSOnExitCell.headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: .zero)
    }
    
    // MARK: Actions Manages
    @IBAction func switchValueChanged(_ sender: Any) {
        // yet to be implemented
        let switchBtn: UISwitch = sender as! UISwitch
        DSOnExitSettingsViewModel.setSwitchButtonSettings(with: DSOnExitSettingsViewModel.OnExitDeleteBtnType(rawValue: switchBtn.tag)!, isOn: switchBtn.isOn)
    }
    
    // MARK: UI Generation Code
    private func generateUI() {
        view.backgroundColor = .white
        let top: UIView = DSNavigationView(title: "On Exit", controller: self)
        loadTableView(navigationView: top)
    }
    
    private func loadTableView(navigationView: UIView) {
        let tableView: UITableView = UITableView()
        let guide = view.safeAreaLayoutGuide
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        
        NSLayoutConstraint.activate(
            [tableView.topAnchor.constraint(equalTo: navigationView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)]
        )
        
    }
    
}
