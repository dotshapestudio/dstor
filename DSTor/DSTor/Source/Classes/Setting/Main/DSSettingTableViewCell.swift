//
//  DSSettingTableViewCell.swift
//  DSTor
//
//  Created by Shahbaz Akram on 28/05/2021.
//

import UIKit
import RxSwift
import RxCocoa

private struct DSSecurityCellViewModel {
    var title:String
    var subTitle:String?
    var switchValue:Bool
    var hideSwitch: Bool
    var accessoryType:UITableViewCell.AccessoryType
    
    init(title:String,_ subTitle:String? = "",switchValue:Bool = false ,hideSwitch:Bool,accessoryType:UITableViewCell.AccessoryType) {
        self.title = title
        self.subTitle = subTitle
        self.switchValue = switchValue
        self.hideSwitch = hideSwitch
        self.accessoryType = accessoryType
    }
}

class DSSettingTableViewCell: UITableViewCell {
    
    var switchBt: UISwitch!
    var subTitleLbl: UILabel!
    var titleLbl: UILabel!
    
    var switchClousre:((_ isOne:Bool)->())!
    
    class var rowHeight: CGFloat {
        return 50
    }
    
    class var headerHeight: CGFloat {
        return 30
    }
    
    // MARK: Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        makeCell()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        makeCell()
    }

    // MARK: Super Class Functions
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    private func assignData(data:DSSecurityCellViewModel) {
        titleLbl.text = data.title
        switchBt.isHidden = data.hideSwitch
        subTitleLbl.text = data.subTitle
        accessoryType = data.accessoryType
        switchBt.isOn = data.switchValue
    }
    
    func setSettings(_ data:SettingsCellData, withViewModel vm: DSSettingsViewModel) -> DSSettingTableViewCell {
        titleLbl.text = data.title
        subTitleLbl.text = data.subTitle
        
        if data.cellType == .checkmark {
            switchBt.isHidden = true
            self.accessoryType = (data.title == vm.currenlyUsedTabSettings.description) ? .checkmark : .none
        } else if data.cellType == .disclousure {
            switchBt.isHidden = true
            self.accessoryType = .disclosureIndicator
        } else if data.cellType == .swithBtn {
            switchBt.isHidden = false
            self.accessoryType = .none
            switchBt.isOn = vm.getSwicthValue(with: data.swType)
        }
        
        self.tintColor = .getColor(.DSPurple)
        return self
    }
    
    static func action(cellData: SettingsCellData, isOn:Bool, andViewModel vm: DSSettingsViewModel) {
        vm.setSwitchButtonSettings(with: cellData.swType, isOn: isOn)
    }
}
