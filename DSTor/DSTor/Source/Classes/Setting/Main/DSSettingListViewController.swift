//
//  DSSettingListViewController.swift
//  DSTor
//
//  Created by Shahbaz Akram on 28/05/2021.
//

import UIKit
import RxSwift

class DSSettingListViewController: UIViewController,DSSettingListViewControllerDelegate {

    // MARK:- Properties
    
    let tableView: UITableView = UITableView()
    private let disposeBag = DisposeBag()
    var viewModel: DSSettingsViewModel = DSSettingsViewModel()
    
    
    // MARK:- LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateUI()
        tableViewconfigure()
    }
    
    // MARK: - DSSettingListViewControllerDelegate
    func needsReload() {
        tableView.reloadData()
    }
    
    // MARK:- PUBLIC Methods
    public func switchBind(cell:DSSettingTableViewCell, withData data: SettingsCellData) {
        cell.switchBt.rx.isOn.changed //when state changed
                    .distinctUntilChanged().asObservable()
            //take signal if state is different than before. This is optional depends on your use case
                    .subscribe(onNext:{ value in
                        DSSettingTableViewCell.action(cellData: data, isOn: value, andViewModel: self.viewModel)
                    }).disposed(by: disposeBag)
    }
    
    // MARK: - Class Methods
    class func instantiate() -> UINavigationController {
        return UINavigationController(rootViewController: self.init())
    }
    
    class var identifier: String {
        return String(describing: self)
    }
}
