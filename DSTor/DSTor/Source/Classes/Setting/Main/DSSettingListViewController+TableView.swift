//
//  DSSettingListViewController+TableView.swift
//  DSTor
//
//  Created by Shahbaz Akram on 28/05/2021.
//

import Foundation

extension DSSettingListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableViewconfigure() {
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DSSettingTableViewCell()
        let data = DSSettingsViewModel.allSettings[indexPath.section].cells[indexPath.row]
        switchBind(cell: cell, withData: data)
        viewModel.errorBindingSwitchButton = { (type: SwitchBtnType) in
            DispatchQueue.main.async { [weak self] in
                var message: String = ""
                if type == .passcode {
                    message = "Error Setting Passcode. May be your device does not setup pascode."
                }
                self?.showAlertWithOKCancel(title: "Error", message: message, okAction: { [weak self] in
                    self?.needsReload()
                }, cancelAction: nil)
            }
        }
        return cell.setSettings(data, withViewModel: viewModel)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        DSSettingsViewModel.allSettings[section].cells.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return DSSettingSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DSSettingTableViewCell.rowHeight
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return DSSettingsViewModel.allSettings[section].sectionTitle
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return DSSettingTableViewCell.headerHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionData = DSSettingsViewModel.allSettings[indexPath.section]
        let data = sectionData.cells[indexPath.row]
        
        if data.cellType == .checkmark && sectionData.sectionID == .onStartup  {
            viewModel.setTabConfigrationSettings(str: data.title)
            tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        } else if data.cellType == .disclousure {
            switch data.newControllerType {
            case .bridge:
                let controller: DSBridgesVC = DSBridgesVC()
                controller.delegate = self
                self.present(controller, animated: true)
            case .exitSettings:
                let controller: DSOnExitSettingsVC = DSOnExitSettingsVC()
                self.present(controller, animated: true)
            case .searchEngine:
                let controller: DSContentPolicyVC = DSContentPolicyVC()
                controller.viewModel.contentType = .searchEngine
                controller.delegate = self
                self.present(controller, animated: true)
            case .userAgent:
                let controller: DSContentPolicyVC = DSContentPolicyVC()
                controller.viewModel.contentType = .userAgent
                controller.delegate = self
                self.present(controller, animated: true)
            case .none:
                break
            }
        }
        
        
        
    }
}

