//
//  DSSettingsViewModel.swift
//  DSTor
//
//  Created by Faizan Siddiqui on 11/08/2021.
//

class DSSettingsViewModel {
    
    class var allSettings: Array<SettingsSectionData> {
        var data: Array<SettingsSectionData> = Array<SettingsSectionData>()
        
        data.append(SettingsSectionData(withSectionID: .onStartup, alongWithtitle: DSSettingSections.title(.onStartup)(), andCells: startupSectionCells()))
        data.append(SettingsSectionData(withSectionID: .security, alongWithtitle: DSSettingSections.title(.security)(), andCells: securitySectionCells()))
        data.append(SettingsSectionData(withSectionID: .general, alongWithtitle: DSSettingSections.title(.general)(), andCells: generalSectionCells()))
        data.append(SettingsSectionData(withSectionID: .activeContentPolicy, alongWithtitle: DSSettingSections.title(.activeContentPolicy)(), andCells: activeContentPolicySectionCells()))
        data.append(SettingsSectionData(withSectionID: .privacy, alongWithtitle: DSSettingSections.title(.privacy)(), andCells: privacySectionCells()))
        
        return data
    }
    
    var errorBindingSwitchButton: ((_ type: SwitchBtnType) -> Void)?
    

    var currenlyUsedTabSettings: TabProtection.stage = Configrations.tabProtection

    func setTabConfigrationSettings(str: String) {
        let results: TabProtection.stage = stageFromDescription(str: str)
        Configrations.tabProtection = results
        currenlyUsedTabSettings =  Configrations.tabProtection
    }
    
    func setSwitchButtonSettings(with switchType: SwitchBtnType, isOn:Bool) {
        switch switchType {
        case .passcode:
            let addedValue = getSwicthValue(with: .passcode)
            
            if isOn {
                _ = ProtectedEnclave.generate_key() != nil
            } else {
                _ = ProtectedEnclave.delete_key()
            }
            
            let newValue = getSwicthValue(with: .passcode)
            
            if newValue == addedValue {
                if let error = errorBindingSwitchButton {
                    error(.passcode)
                }
            }
        case .javascript:
            if isOn {
                updateSecurityPresents(secType: .medium)
            } else {
                updateSecurityPresents(secType: .insecure)
            }
        case .browsingHistory:
            Configrations.findLive = isOn
        default:
            break
        }
    }
        
    func getSwicthValue(with switchType: SwitchBtnType) -> Bool {
        switch switchType {
        case .passcode:
            return ProtectedEnclave.add_key() != nil
        case .javascript:
            return PredeterminedProtection(HostConfigrations.byDefault()) == .medium
        case .browsingHistory:
            return Configrations.findLive
        default:
            return false
        }
    }
    
    class var userAgents: Array<String> {
        let arr: Array<String> = ["Standard", "Normalized iPhone (iOS Safari)", "Normalized iPad (iOS Safari)", "Windows 7 (NT 6.1), Firefox 24", "Mac OS X 10.9.2, Safari 7.0.3"]
        return arr
    }
    
    class var tabSecurity: Array<TabProtection.stage> {
        let arr: Array<TabProtection.stage> = [TabProtection.stage.forgetOnShutdown, TabProtection.stage.alwaysRemember, TabProtection.stage.clearOnBackground]
        return arr
    }
    
    // MARK: Private DataMembers and Functions
    
    func updateSecurityPresents(secType: PredeterminedProtection) {
        if let values = secType.data {
            let hostSettings: HostConfigrations = HostConfigrations.byDefault()
            hostSettings.option_contentPolicy = values.csp
            hostSettings.option_webRTC = values.webRtc
            hostSettings.option_mixedModeResources = values.mixedMode
            hostSettings.saveCopy().add()
        }
    }
    
    private func stageFromDescription(str: String) -> TabProtection.stage {
        switch str {
        case TabProtection.stage.alwaysRemember.description:
            return .alwaysRemember
        case TabProtection.stage.forgetOnShutdown.description:
            return .forgetOnShutdown
        case TabProtection.stage.clearOnBackground.description:
            return .clearOnBackground
        default:
            return .alwaysRemember
        }
    }
    
    private static func startupSectionCells() -> Array<SettingsCellData> {
        var arr: Array<SettingsCellData> = Array<SettingsCellData>()
        
        for tab in tabSecurity {
            let cell: SettingsCellData = SettingsCellData(withTitle: tab.description, subTitle: "", andCellType: .checkmark)
            arr.append(cell)
        }
        
        return arr
    }
    
    private static func securitySectionCells() -> Array<SettingsCellData> {
        var arr: Array<SettingsCellData> = Array<SettingsCellData>()
        
        let cell: SettingsCellData = SettingsCellData(withTitle: "Passcode lock / Face ID", subTitle: "", andCellType: .swithBtn, optionalSwitchType: .passcode)
        arr.append(cell)
        
        return arr
    }
    
    private static func generalSectionCells() -> Array<SettingsCellData> {
        var arr: Array<SettingsCellData> = Array<SettingsCellData>()
        
        let subTitle: DSBridgesVM.BridgeMap = DSBridgesVM.section1Data.filter { bridge in
            bridge.type == Configrations.recentUsedTorBrigeType
        }.first!
        
        let cell1: SettingsCellData = SettingsCellData(withTitle: "Bridge Configration", subTitle: subTitle.strType, andCellType: .disclousure, andNewControllerType: .bridge)
        let cell2: SettingsCellData = SettingsCellData(withTitle: "User-Agent", subTitle: HostConfigrations.byDefault().option_userAgent, andCellType: .disclousure, andNewControllerType: .userAgent)
        let cell3: SettingsCellData = SettingsCellData(withTitle: "Search Engine", subTitle: Configrations.searchEngineTitle, andCellType: .disclousure, andNewControllerType: .searchEngine)
        
        arr.append(cell1)
        arr.append(cell2)
        arr.append(cell3)
        
        return arr
    }
    
    private static func activeContentPolicySectionCells() -> Array<SettingsCellData> {
        var arr: Array<SettingsCellData> = Array<SettingsCellData>()
        
        let cell: SettingsCellData = SettingsCellData(withTitle: "JavaScript", subTitle: "", andCellType: .swithBtn, optionalSwitchType: .javascript)
        arr.append(cell)
        
        return arr
    }
    
    private static func privacySectionCells() -> Array<SettingsCellData> {
        var arr: Array<SettingsCellData> = Array<SettingsCellData>()
        
        let cell1: SettingsCellData = SettingsCellData(withTitle: "On Exit Settings", subTitle: "", andCellType: .disclousure, andNewControllerType: .exitSettings)
        let cell2: SettingsCellData = SettingsCellData(withTitle: "Browsing History", subTitle: "", andCellType: .swithBtn, optionalSwitchType: .browsingHistory)
        
        arr.append(cell1)
        arr.append(cell2)
        
        return arr
    }
}
