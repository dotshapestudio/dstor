//
//  DSSettingListViewController+UI.swift
//  DSTor
//
//  Created by Shahbaz Akram on 27/07/2021.
//

import Foundation

extension DSSettingListViewController {
    func generateUI() {
        view.backgroundColor = .white
        let top: UIView = DSNavigationView(title: "Settings", controller: self)
        addTableView(topView: top)
    }
    
    private func addTableView(topView: UIView) {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .singleLine
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.view.addSubview(tableView)
        
        // MARK: TableView Constarints
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.topAnchor.constraint(equalTo: topView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
