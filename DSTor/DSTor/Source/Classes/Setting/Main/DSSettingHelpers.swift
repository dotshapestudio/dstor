//
//  DSSettingHelpers.swift
//  DSTor
//
//  Created by Shahbaz Akram on 01/06/2021.
//

import Foundation

protocol DSSettingListViewControllerDelegate {
    func needsReload()
}

enum CellType: Int {
    case checkmark = 0, swithBtn, disclousure
}

enum SwitchBtnType: Int {
    case none = 0, passcode, javascript, browsingHistory
}

enum NewControllerType: Int {
    case none = 0, bridge, userAgent, searchEngine, exitSettings
}

enum DSSettingSections:Int,CaseIterable {
    case onStartup = 0, security, general, activeContentPolicy, privacy
    
    func title() -> String {
        switch self {
        case .onStartup:
            return "On startup"
        case .security:
            return "Security"
        case .general:
            return "General"
        case .activeContentPolicy:
            return "Active Content Policy"
        case .privacy:
            return "Privacy"
        }
    }
}

@objcMembers

class SettingsCellData: NSObject {
    
    let title: String
    let subTitle: String
    let cellType: CellType
    let swType: SwitchBtnType
    let newControllerType: NewControllerType

    init(withTitle title: String, subTitle sub: String, andCellType cellType: CellType, optionalSwitchType switchBtn: SwitchBtnType = .none, andNewControllerType newControllerType: NewControllerType = .none) {
        self.title = title
        self.subTitle = sub
        self.cellType = cellType
        self.swType = switchBtn
        self.newControllerType = newControllerType
    }
}

class SettingsSectionData: NSObject {
    
    let sectionID: DSSettingSections
    let sectionTitle: String
    let cells: Array<SettingsCellData>
   

    init(withSectionID id: DSSettingSections, alongWithtitle title: String, andCells cells: Array<SettingsCellData>) {
        self.sectionID = id
        self.sectionTitle = title
        self.cells = cells
    }
}
