//
//  DSSettingTableViewCell+UI.swift
//  DSTor
//
//  Created by Macbook on 28/07/2021.
//

import Foundation

extension DSSettingTableViewCell {
    
    func makeCell() {
        titleLbl = getLabel(font: .systemFont(ofSize: 16), alignment: .natural)
        subTitleLbl = getLabel(font: .systemFont(ofSize: 13), alignment: .right)
        switchBt = getSwitch()
        
        selectionStyle = .none
        subTitleLbl.numberOfLines = 0
        titleLbl.numberOfLines = 2
        
        contentView.addSubview(titleLbl)
        contentView.addSubview(subTitleLbl)
        contentView.addSubview(switchBt)
        
        NSLayoutConstraint.activate([
            titleLbl.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            titleLbl.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            switchBt.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -30),
            switchBt.centerYAnchor.constraint(equalTo: titleLbl.centerYAnchor),
            subTitleLbl.centerYAnchor.constraint(equalTo: titleLbl.centerYAnchor),
            subTitleLbl.leadingAnchor.constraint(equalTo: titleLbl.trailingAnchor),
            subTitleLbl.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            subTitleLbl.widthAnchor.constraint(equalToConstant: 130)
        ])
    }
    
    private func getLabel(font f: UIFont, alignment a: NSTextAlignment) -> UILabel {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = f
        label.textAlignment = a
        label.textColor = .black
        label.contentMode = .left
        
        return label
    }
    
    private func getSwitch() -> UISwitch {
        let btnSwitch: UISwitch = UISwitch()
        btnSwitch.translatesAutoresizingMaskIntoConstraints = false
        btnSwitch.isOn = true
        btnSwitch.onTintColor = UIColor.getColor(.DSPurple)
        btnSwitch.thumbTintColor = .white
        btnSwitch.contentHorizontalAlignment = .center
        btnSwitch.contentVerticalAlignment = .center
        btnSwitch.isEnabled = true
        
        return btnSwitch
    }
}
