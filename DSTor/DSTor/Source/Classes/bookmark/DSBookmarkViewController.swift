//
//  DSBookmarkViewController.swift
//  DSTor
//
//  Created by Shahbaz Akram on 28/05/2021.
//

import UIKit

class DSBookmarkViewController: UIViewController {
    
    var saveBt: UIButton!
    var urlTF: UITextField!
    var titleTF: UITextField!
    var index: Int?
    private var bookmark: DSBookmark?
    var delegate: DSBookmarkListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async { [weak self] in
            self?.generateUI()
            
            // Was called via "+" (add).
            if self?.index == nil {
                // Check, if we have a valid URL in the current tab. If so, prefill with that.
                if let info = DSBookmarkViewController.getCurrentTabInfo() {
                    self?.titleTF.text = info.title
                    self?.urlTF.text = info.url.absoluteString
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
                if index != nil {
                    bookmark?.title = titleTF.text

                    // Don't allow empty URL.
                    if let url = cleanUrl() {
                        bookmark?.link = url
                    }

        //            bookmark?.icon = favIconRow.value

                    DSBookmark.save()

//                    if let bookmark = bookmark {
//                        Nextcloud.store(bookmark, id: nextcloudId)
//                    }

                    delegate?.needsReload()

//                    sceneGoneStoreImmediately = true
                }
    }
    
    class var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func action(_ sender: UIButton) {
        if sender == saveBt {
            // Store changes, if user edits an existing bookmark or bookmark was created
            // with #addNew.
            addNew()
        }
    }
    
    public class func getCurrentTabInfo() -> (url: URL, title: String?)? {
        if let tab = AppDelegate.shared?.browsingUi?.usedTab,
           let scheme = tab.link.scheme?.lowercased() {
            
            if scheme == "http" || scheme == "https" {
                return (url: tab.link, title: tab.name)
            }
        }
        
        return nil
    }
    
    @objc private func addNew() {
        if handleFieldsValidationsWithSuccess() {
            bookmark = DSBookmark()

            DSBookmark.allBMs.append(bookmark!)
            // Trigger store in #viewWillDisappear by setting index != nil.
            index = DSBookmark.allBMs.firstIndex(of: bookmark!)
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func handleFieldsValidationsWithSuccess() -> Bool {
        var msg: String = ""
        var result: Bool = true
        
        if (titleTF.text?.isEmpty ?? true) && (urlTF.text?.isEmpty ?? true) {
            msg = "Title and URL are Mandatory"
            result = false
            
        } else if (titleTF.text?.isEmpty ?? true) {
            msg = "Title is Mandatory"
            result = false
        } else if (urlTF.text?.isEmpty ?? true) {
            msg = "URL is Mandatory"
            result = false
        } else if !(urlTF.text?.contains(".") ?? false) || (urlTF.text?.last == ".") || (urlTF.text?.first == "."){
            msg = "URL is Invalid"
            result = false
        }
        
        if !result {
            self.showAlert(title: "Error", message: msg)
        }
        
        return result
    }
    
    private func cleanUrl() -> URL? {
        
        if urlTF.text == "" {
            return nil
        }

        guard let url = URL(string: urlTF.text!) else { return nil }
        var urlc = URLComponents(url: url , resolvingAgainstBaseURL: true)
        if urlc?.scheme?.isEmpty ?? true {
            urlc?.scheme = "https"
        }

        // When no URL formatting is given, a domain is always parsed as a path.
        // Users enter domains here most likely, though.
        if urlc?.host?.isEmpty ?? true {
            let host = urlc?.path
            urlc?.host = host
            urlc?.path = "/"
        }

        return urlc?.url
    }
}

extension DSBookmarkViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(#function)
    }
}
