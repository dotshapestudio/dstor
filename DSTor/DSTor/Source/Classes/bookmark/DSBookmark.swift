//
//  Bookmark.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 08.10.19.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//


import UIKit
import FavIcon

@objc
@objcMembers
open class DSBookmark: NSObject {

	static let imgDefault = UIImage(systemName: "safari")//UIImage(named: "default-icon")!

	private static let keyForBM = "bookmarks"
	private static let keyForVers = "version"
	private static let keyForTitle = "name"
	private static let keyForLink = "url"
	private static let keyForImage = "icon"

	private static let vers = 2

	private static var base = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
	private static var filePathForBM = base?.appendingPathComponent("bookmarks.plist")

	private static let defaultBMs: [DSBookmark] = {
		var defaults = [DSBookmark]()

		defaults.append(DSBookmark(name: "DuckDuckGo", url: "https://3g2upl4pq6kufc4m.onion/"))
		defaults.append(DSBookmark(name: "New York Times", url: "https://mobile.nytimes3xbfgragh.onion/"))
		defaults.append(DSBookmark(name: "BBC", url: "https://bbcnewsv2vjtpsuy.onion/"))
		defaults.append(DSBookmark(name: "Facebook", url: "https://m.facebookcorewwwi.onion/"))
		defaults.append(DSBookmark(name: "ProPublica", url: "https://www.propub3r6espa33w.onion/"))
		defaults.append(DSBookmark(name: "Freedom of the Press Foundation", url: "https://freedom.press/"))

		defaults.append(DSBookmark(name: "Onion Browser landing page", url: "http://3heens4xbedlj57xwcggjsdglot7e36p4rogy642xokemfo2duh6bbyd.onion/"))
		defaults.append(DSBookmark(name: "Onion Browser official site", url: "https://onionbrowser.com"))
		defaults.append(DSBookmark(name: "The Tor Project", url: "http://expyuzz4wqqyqhjn.onion/"))

		return defaults
	}();

	private static var defaultWebPageNeedsRefresh = true

	static var allBMs: [DSBookmark] = {

		// Init FavIcon config here, because all code having to do with bookmarks should come along here anyway.
		FavIcon.downloadSession = URLSession.shared
		FavIcon.authorize = { url in
			JAHPAuthenticatingHTTPProtocol.temporarilyAllow(url)

			return true
		}

		var bookmarks = [DSBookmark]()

		if let path = filePathForBM {
			let data = NSDictionary(contentsOf: path)

			for b in data?[keyForBM] as? [NSDictionary] ?? [] {
				bookmarks.append(DSBookmark(b))
			}
		}

		return bookmarks
	}()

	class func prepareForFirstTimeRun() {
		if Configrations.firstTimeBookmarkCompletelyRun {
			return
		}

		// Only set up default list of bookmarks, when there's no others.
		if allBMs.count < 1 {
			allBMs.append(contentsOf: defaultBMs)

			save()

			DispatchQueue.global(qos: .background).async {
				for bookmark in allBMs {
					bookmark.getImage() {
						save()
					}
				}
				
				Configrations.firstTimeBookmarkCompletelyRun = true
			}
        }
	}

	@discardableResult
	class func append(_ name: String?, _ url: String) -> DSBookmark {
		let bookmark = DSBookmark(name: name, url: url)

		allBMs.append(bookmark)

		return bookmark
	}

	@discardableResult
	class func save() -> Bool {
		if let path = filePathForBM {

			// Trigger update of start page when things changed.
			defaultWebPageNeedsRefresh = true

			for tab in AppDelegate.shared?.browsingUi?.tabList ?? [] {
				if tab.link == URL.start {
					DispatchQueue.main.async {
						tab.reload()
					}
				}
			}

			let bookmarks = NSMutableArray()

			for b in allBMs {
				bookmarks.add(b.convertToDictionary())
			}

			let data = NSMutableDictionary()
			data[keyForBM] = bookmarks
			data[keyForVers] = vers
			
			return data.write(to: path, atomically: true)
		}

		return false
	}

	@objc(containsUrl:)
	class func isAlreadyPresent(url: URL) -> Bool {
		return allBMs.contains { $0.link == url }
	}

	var title: String?
	var link: URL?

	private var imageName = ""

	private var _image: UIImage?
	var image: UIImage? {
		get {
			if _image == nil && !imageName.isEmpty,
				let path = DSBookmark.base?.appendingPathComponent(imageName).path {

				_image = UIImage(contentsOfFile: path)
			}

			return _image
		}
		set {
			_image = newValue

			// Remove old icon, if it gets deleted.
			if _image == nil {
				if !imageName.isEmpty,
					let path = DSBookmark.base?.appendingPathComponent(imageName) {

					try? FileManager.default.removeItem(at: path)
				}

				imageName = ""
			}

			if _image != nil {
				if imageName.isEmpty {
					imageName = UUID().uuidString
				}

				if let path = DSBookmark.base?.appendingPathComponent(imageName) {
					try? _image?.pngData()?.write(to: path)
				}
			}
		}
	}

	init(name: String? = nil, url: String? = nil, icon: UIImage? = nil) {
		super.init()

		self.title = name

		if let url = url {
			self.link = URL(string: url)
		}

		self.image = icon
	}

	init(name: String?, url: String?, iconName: String) {
		super.init()

		self.title = name

		if let url = url {
			self.link = URL(string: url)
		}

		self.imageName = iconName
	}

	convenience init(_ dic: NSDictionary) {
		self.init(name: dic[DSBookmark.keyForTitle] as? String,
				  url: dic[DSBookmark.keyForLink] as? String,
				  iconName: dic[DSBookmark.keyForImage] as? String ?? "")
	}


	// MARK: Public Methods

	class func image(for url: URL, _ completion: @escaping (_ image: UIImage?) -> Void) {
		try! FavIcon.downloadPreferred(url, width: 128, height: 128) { result in
			if case let .success(image) = result {
				completion(image)
			}
			else {
				completion(nil)
			}
		}

	}

	func getImage(_ completion: @escaping () -> Void) {
		if let url = link {
			DSBookmark.image(for: url) { image in
				self.image = image

				completion()
			}
		}
	}


	// MARK: Private Methods

	private func convertToDictionary() -> NSDictionary {
		return NSDictionary(dictionary: [
			DSBookmark.keyForTitle: title ?? "",
			DSBookmark.keyForLink: link?.absoluteString ?? "",
			DSBookmark.keyForImage: imageName,
		])
	}
}
