//
//  DSBookmark+TableView.swift
//  DSTor
//
//  Created by Shahbaz Akram on 27/05/2021.
//

import Foundation

extension DSBookmarkListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableViewconfigure() {
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if DSBookmark.allBMs.count == 0 {
            noDataView.frame = tableView.frame
            tableView.backgroundView = noDataView
        } else {
            tableView.backgroundView = nil
        }
        return DSBookmark.allBMs.count//(isFiltering ? filtered : Bookmark.all).count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return DSCellForBM.height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = DSCellForBM(bookmark: DSBookmark.allBMs[indexPath.row])
        
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return !isFiltering
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return !isFiltering
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DSBookmark.allBMs[indexPath.row].image = nil // Delete icon file.
            let bookmark = DSBookmark.allBMs.remove(at: indexPath.row)
            DSBookmark.save()
//            Nextcloud.delete(bookmark)
            tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        }
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        DSBookmark.allBMs.insert(DSBookmark.allBMs.remove(at: sourceIndexPath.row), at: destinationIndexPath.row)
        DSBookmark.save()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var index: Int? = indexPath.row

        if isFiltering {
            index = DSBookmark.allBMs.firstIndex(of: filtered[index!])
        }

        if let index = index {
            if tableView.isEditing {
                let vc = DSBookmarkViewController()
                vc.delegate = self
                vc.index = index

                navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let bookmark = DSBookmark.allBMs[index]

                AppDelegate.shared?.browsingUi?.insertTab(
                bookmark.link, transition: .notAnimated) { _ in
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }

        tableView.deselectRow(at: indexPath, animated: false)
    }
}
