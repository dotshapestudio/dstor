//
//  BookmarkCell.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 19.12.19.
//  Copyright © 2019 jcs. All rights reserved.
//

import UIKit

class DSCellForBM: UITableViewCell {

	class var height: CGFloat {
		return 44
	}

	var imgView: UIImageView!
    var lblName: UILabel!

    init(bookmark: DSBookmark) {
        super.init(style: .default, reuseIdentifier: nil)
        
        DispatchQueue.main.async { [weak self] in
            self?.generateUI()
            self?.imgView.image = UIImage(systemName: "safari")//bookmark.icon
            self?.lblName.text = bookmark.title?.isEmpty ?? true
                ? bookmark.link?.absoluteString
                : bookmark.title
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
