//
//  DSBookmarkViewController.swift
//  DSTor
//
//  Created by Shahbaz Akram on 27/05/2021.
//

import UIKit

protocol DSBookmarkListViewControllerDelegate {
    func needsReload()
}

class DSBookmarkListViewController: UIViewController, DSBookmarkListViewControllerDelegate {
    var noDataView: UIView!
    // MARK: - PROPERTIES
    var tableView: UITableView!
//    var _needsReload = false
    var isFiltering: Bool {
        return searchController.isActive
            && !(searchController.searchBar.text?.isEmpty ?? true)
    }
    let searchController = UISearchController(searchResultsController: nil)
    var filtered = [DSBookmark]()
    
    // MARK: - Controller lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DSBookmark.prepareForFirstTimeRun()
        
        DispatchQueue.main.async { [weak self] in
            self?.generateUI()
            self?.tableViewconfigure()
        }
    }
    
    // MARK:-  ACTION
    
    @IBAction func action(_ sender: UIButton) {
        let vc = DSBookmarkViewController()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: - DSBookmarkListViewControllerDelegate
    func needsReload() {
        tableView.reloadData()
    }
}


