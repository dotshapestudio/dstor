//
//  DSBookmarkListViewController+UI.swift
//  DSTor
//
//  Created by Macbook on 28/07/2021.
//

import Foundation

extension DSBookmarkListViewController {
    
    func generateUI() {
        view.backgroundColor = .white
        
        makeEmptyBookmarkView()
        makeController()
    }
    
    private func makeController() {
        let tooBar: UIView = getTopBar()
        tableView = getTableView()
        
        view.addSubview(tooBar)
        view.addSubview(tableView)
        
        let guide: UILayoutGuide = view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            tooBar.topAnchor.constraint(equalToSystemSpacingBelow: guide.topAnchor, multiplier: 1.0),
            tooBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tooBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tooBar.bottomAnchor.constraint(equalTo: tableView.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func getTopBar() -> UIView {
        let toolBar: UIView = DSNavigationView(title: "Bookmarks", controller: self)
        
        let addBtn: UIButton = getButtonAction()
        addBtn.addTarget(self, action: #selector(action(_:)), for: .touchUpInside)
        
        toolBar.addSubview(addBtn)
        
        NSLayoutConstraint.activate([
            addBtn.leadingAnchor.constraint(equalTo: toolBar.leadingAnchor, constant: 15),
            addBtn.centerYAnchor.constraint(equalTo: toolBar.centerYAnchor)
        ])
        
        return toolBar
    }
    
    private func getTableView() -> UITableView {
        let table: UITableView = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.showsVerticalScrollIndicator = false
        table.showsHorizontalScrollIndicator = false
        table.delegate = self
        table.dataSource = self
        
        return table
    }
    
    private func makeEmptyBookmarkView() {
        noDataView = UIView()
        noDataView.backgroundColor = .clear
        
        let img: UIImageView = getImageView()
        let lbl: UILabel = getLabel()
        
        noDataView.addSubview(img)
        noDataView.addSubview(lbl)
        
        NSLayoutConstraint.activate([
            img.widthAnchor.constraint(equalToConstant: 80),
            img.heightAnchor.constraint(equalToConstant: 150),
            img.bottomAnchor.constraint(equalTo: lbl.topAnchor, constant: -8),
            img.centerXAnchor.constraint(equalTo: noDataView.centerXAnchor),
            lbl.leadingAnchor.constraint(equalTo: noDataView.leadingAnchor, constant: 20),
            lbl.trailingAnchor.constraint(equalTo: noDataView.trailingAnchor, constant: -20),
            lbl.bottomAnchor.constraint(equalTo: noDataView.bottomAnchor, constant: -(view.safeAreaInsets.bottom + 48))
        ])
    }
    
    private func getImageView() -> UIImageView {
        let imageView: UIImageView = UIImageView(image: UIImage(systemName: "bookmark.slash"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .getColor(.DSPurple)
        
        return imageView
    }
    
    private func getLabel() -> UILabel {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .getColor(.DSPurple)
        label.text = "You don't have bookmarks yet. All your bookmarks will show up here"
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 17)
        label.numberOfLines = 0
        
        return label
    }
    
    private func getButtonAction() -> UIButton {
        let btn: UIButton = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .white
        btn.setImage(.init(systemName: "plus"), for: .normal)
        btn.layer.cornerRadius = 12.5
        btn.layer.masksToBounds = true
        btn.tintColor = .black
        
        // Constraints
        NSLayoutConstraint.activate([
            btn.widthAnchor.constraint(equalToConstant: 25),
            btn.heightAnchor.constraint(equalToConstant: 25)
        ])
        
        return btn
    }
}
