//
//  DSCellForBM+UI.swift
//  DSTor
//
//  Created by Macbook on 29/07/2021.
//

import Foundation

extension DSCellForBM {
    
    func generateUI() {
        makeCell()
    }
    
    private func makeCell() {
        lblName = getLabel()
        imgView = getImageView()
        
        contentView.addSubview(lblName)
        contentView.addSubview(imgView)
        
        NSLayoutConstraint.activate([
            imgView.widthAnchor.constraint(equalToConstant: 32),
            imgView.heightAnchor.constraint(equalToConstant: 32),
            imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
            imgView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            imgView.trailingAnchor.constraint(equalTo: lblName.leadingAnchor, constant: -8),
            lblName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            lblName.centerYAnchor.constraint(equalTo: imgView.centerYAnchor)
        ])
    }
    
    private func getImageView() -> UIImageView {
        let imageView: UIImageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.tintColor = .getColor(.DSPurple)
        
        return imageView
    }
    
    private func getLabel() -> UILabel {
        let label: UILabel = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .natural
        label.font = .systemFont(ofSize: 17)
        label.numberOfLines = 1
        
        return label
    }
}
