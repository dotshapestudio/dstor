//
//  DSBookmarkViewController+UI.swift
//  DSTor
//
//  Created by Macbook on 28/07/2021.
//

import Foundation

extension DSBookmarkViewController {
    
    func generateUI() {
        view.backgroundColor = .white
        
        makeController()
    }
    
    private func makeController() {
        let stackView = getStackView()
        let toolBar: UIView = DSNavigationView(title: "Add Bookmark", controller: self)
        
        saveBt = getButton(withText: "Save")
        saveBt.layer.cornerRadius = 5
        saveBt.layer.masksToBounds = true
        
        toolBar.addSubview(saveBt)
        
        NSLayoutConstraint.activate([
            saveBt.leadingAnchor.constraint(equalTo: toolBar.leadingAnchor, constant: 15),
            saveBt.centerYAnchor.constraint(equalTo: toolBar.centerYAnchor),
            saveBt.widthAnchor.constraint(equalTo: saveBt.titleLabel!.widthAnchor, multiplier: 1.3)
        ])
        
        view.addSubview(stackView)
        view.addSubview(toolBar)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 93),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
        ])
        
        titleTF = getTextField(withPlaceholder: "Title")
        urlTF = getTextField(withPlaceholder: "URL")
        urlTF.keyboardType = .URL
        
        stackView.addArrangedSubview(titleTF)
        stackView.addArrangedSubview(urlTF)
    }
    
    private func getButton(withText text: String) -> UIButton {
        let btn: UIButton = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.contentVerticalAlignment = .center
        btn.contentHorizontalAlignment = .center
        btn.setTitle(text, for: .normal)
        btn.setTitleColor(UIColor.getColor(.DSPurple), for: .normal)
        btn.titleLabel!.font = .systemFont(ofSize: 20)
        btn.addTarget(self, action: #selector(action), for: .touchUpInside)
        btn.backgroundColor = .white
        
        
        return btn
    }
    
    private func getLabel(withText text: String) -> UILabel {
        let lbl: UILabel = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = text
        lbl.font = .systemFont(ofSize: 18, weight: .medium)
        lbl.textColor = .getColor(.DSPurple)
        
        return lbl
    }
    
    private func getTextField(withPlaceholder text: String) -> UITextField {
        let textField: UITextField = UITextField()
        textField.borderStyle = .roundedRect
        textField.clearButtonMode = .never
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.textContentType = .name
        textField.smartDashesType = .no
        textField.smartInsertDeleteType = .no
        textField.smartQuotesType = .no
        textField.spellCheckingType = .no
        textField.keyboardType = .namePhonePad
        textField.returnKeyType = .default
        textField.contentVerticalAlignment = .center
        textField.contentHorizontalAlignment = .left
        textField.textColor = .black
        textField.font = UIFont.systemFont(ofSize: 14)
        textField.placeholder = text
        textField.delegate = self
        
        return textField
    }
    
    private func getStackView() -> UIStackView {
        let stackView: UIStackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 10
        
        return stackView
    }
}
