//
//  DSConnectingTorViewModel.swift
//  DSTor
//
//  Created by Shahbaz Akram on 16/04/2021.
//

import UIKit

class DSConnectingViewModel:DSTorManagerDelegate {

    var updateProgress : ((_ value:Int) -> ()) = {_ in }
    var torConnected : (() -> ()) = {}
    var torConnError : (() -> ()) = {}
    
    private var presets: [PredeterminedProtection] = [.secure, .medium, .insecure]
    
    class func start() {
        let appDelegate = AppDelegate.shared

        if appDelegate?.browsingUi == nil {
            appDelegate?.browsingUi = DSMainWebController()
        }

        appDelegate?.show(appDelegate?.browsingUi) { _ in
            TabProtection.reset()

            appDelegate?.browsingUi?.shouldShow()
        }
    }
    
    func configeration() {
        DSTorManager.shared.setBridgeSettings(bridgesType: Configrations.recentUsedTorBrigeType,
                                                   customBridges: Configrations.ownBridges)

        DSTorManager.shared.startTorBrowser(delegate: self)
    }
    
    func securitySettings() {
        if !Configrations.introAdded {
            let preset = presets[2]

            let hs = HostConfigrations.byDefault()

            hs.option_contentPolicy = preset.data?.csp ?? .strict
            hs.option_webRTC = preset.data?.webRtc ?? false
            hs.option_mixedModeResources = preset.data?.mixedMode ?? false
            hs.option_userAgent = DSSettingsViewModel.userAgents[1]

            // Trigger creation, save and store of default HostSettings.
            hs.saveCopy().add()
            
            Configrations.introAdded = true
        }
    }
    
    
    // MARK: OnionManagerDelegate

    func connection_progress(_ progress: Int) {
        DispatchQueue.main.async {
            self.updateProgress(progress)
        }
    }

    func connection_finished() {
        DispatchQueue.main.async {
            self.securitySettings()
            DSConnectingViewModel.start()
            self.torConnected()
        }
    }

    func connection_difficulties() {
        DispatchQueue.main.async {
            self.torConnError()
        }
    }
    
}
