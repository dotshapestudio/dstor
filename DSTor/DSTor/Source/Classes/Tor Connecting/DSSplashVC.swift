//
//  ConnectingViewController.swift
//  OnionBrowser2
//
//  Created by Benjamin Erhart on 10.01.20.
//  Copyright © 2012 - 2021, Tigas Ventures, LLC (Mike Tigas)
//
//  This file is part of Onion Browser. See LICENSE file for redistribution terms.
//

import UIKit

class DSSplashVC: UIViewController {
    
    // MARK: - Properties
    
    var lblTitle: UILabel!
    var lblForProgress: UILabel!
    var connectionProgress: UIProgressView! {
        didSet {
            connectionProgress.isHidden = true
        }
    }
    
    var viewModel = DSConnectingViewModel()
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
		super.viewDidLoad()
        
        DispatchQueue.main.async {[weak self] in
            self?.view.addGifImage(name: "connecting")
            self?.generateUI()
            self?.viewModelSetup()
        }
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
        
	}
    
    // MARK: - Private Methods
    fileprivate func viewModelSetup() {
        viewModel.configeration()
        
        viewModel.updateProgress = {  value in
            self.connectionProgress.progress = Float(value) / 100
            self.lblForProgress.text = "\(Float(value)) %"
            self.connectionProgress.isHidden = false
            self.lblForProgress.isHidden = false
        }
        
        viewModel.torConnected = {
            // tor connected
        }
        
        viewModel.torConnError = {
            // tor error
        }
    }
}
