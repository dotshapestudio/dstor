//
//  DSSplashVC+UI.swift
//  DSTor
//
//  Created by Macbook on 28/07/2021.
//

import Foundation

extension DSSplashVC {
    
    func generateUI() {
        view.backgroundColor = .lightGray
        
        makeController()
    }
    
    private func makeController() {
        connectionProgress = getProgressView()
        lblForProgress = getLabel(withText: "0%", andFont: .systemFont(ofSize: 17))
        lblForProgress.isHidden = true
        lblTitle = getLabel(withText: "Tor Connecting...", andFont: .systemFont(ofSize: 18))
        
        view.addSubview(connectionProgress)
        view.addSubview(lblForProgress)
        view.addSubview(lblTitle)
        
        NSLayoutConstraint.activate([
            lblForProgress.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(view.safeAreaInsets.bottom + 49)),
            lblForProgress.topAnchor.constraint(equalTo: connectionProgress.bottomAnchor, constant: 8),
            lblForProgress.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lblTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lblTitle.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -143.5),
            connectionProgress.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 36),
            connectionProgress.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -36)
        ])
    }
    
    private func getProgressView() -> UIProgressView {
        let progress: UIProgressView = UIProgressView()
        progress.translatesAutoresizingMaskIntoConstraints = false
        progress.progressViewStyle = .default
        progress.progress = 0
        progress.progressTintColor = .white
        progress.trackTintColor = .lightGray
        progress.tintColor = .white
        
        return progress
    }
    
    private func getLabel(withText text: String, andFont font: UIFont) -> UILabel {
        let lbl: UILabel = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = text
        lbl.font = font
        lbl.textColor = .white
        
        return lbl
    }
}
