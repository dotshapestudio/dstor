//
//  SearchEngine.swift
//  DSTor
//
//  Created by Shahbaz Akram on 16/04/2021.
//

import Foundation

enum SearchEngine:String {
    case google =  "http://google.com/search?q="
    case yahoo = "http://search.yahoo.com/?q="
    case bing = "http://www.bing.com/search?q="
    case ask = "http://www.ask.com/web?q="
    case duckduckGo = "https://duckduckgo.com/?q="
    
    
    // call as function if want to skip rawValue i.e google(), yahoo()
    func callAsFunction() -> String {
        return self.rawValue
    }
}

