//
//  UIColorExtension.swift
//  DSTor
//
//  Created by Shahbaz Akram on 26/07/2021.
//

import Foundation

enum AssetsColor: String {
    case DSPurple
}

extension UIColor {
    static func getColor(_ name: AssetsColor) -> UIColor {
         return UIColor(named: name.rawValue)!
    }
}
