//
//  DSImage.swift
//  DSTor
//
//  Created by Shahbaz Akram on 04/06/2021.
//

import UIKit

extension UIImage {
    enum AssetIdentifier:String {
        case silver = "silver"
        case silverSelected = "silver-selected"
        case gold = "gold"
        case goldSelected = "gold-selected"
        case bronze = "bronze"
        case bronzeSelected = "bronze-selected"
    }
    
    convenience init!(assetIdentifier:AssetIdentifier) {
        self.init(named:assetIdentifier.rawValue)
    }
}
