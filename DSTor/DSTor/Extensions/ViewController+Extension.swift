//
//  ViewController+Extension.swift
//  DSTor
//
//  Created by Shahbaz Akram on 16/04/2021.
//

import UIKit

extension UIViewController {
  func showAlert(title: String, message: String) {
    let alertController = UIAlertController(title: title, message:
      message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
    }))
    self.present(alertController, animated: true, completion: nil)
  }
    
    func showAlertWithOKCancel(title: String, message: String, okAction: @escaping (() -> Void), cancelAction:  (() -> Void)? = nil) {
      let alertController = UIAlertController(title: title, message:
        message, preferredStyle: .alert)
      alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
        okAction()
      }))
        if let cancel = cancelAction {
            alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {action in
                cancel()
            }))
        }
      self.present(alertController, animated: true, completion: nil)
    }

}

extension UIViewController {
    
    public var top: UIViewController {
        if let vc = subViewController {
            return vc.top
        }

        return self
    }

    public var subViewController: UIViewController? {
        if let vc = self as? UINavigationController {
            return vc.topViewController
        }

        if let vc = self as? UISplitViewController {
            return vc.viewControllers.last
        }

        if let vc = self as? UITabBarController {
            return vc.selectedViewController
        }

        if let vc = presentedViewController {
            return vc
        }

        return nil
    }

    /**
    Presents a view controller modally animated.

    Does it as a popover, when a `sender` object is provided.

    - parameter vc: The view controller to present.
    - parameter sender: The `UIView` with which the user triggered this operation.
    */
    func present(_ vc: UIViewController, _ sender: UIView? = nil) {
        if let sender = sender {
            vc.modalPresentationStyle = .popover
            vc.popoverPresentationController?.sourceView = sender.superview
            vc.popoverPresentationController?.sourceRect = sender.frame

            if let delegate = vc as? UIPopoverPresentationControllerDelegate {
                vc.popoverPresentationController?.delegate = delegate
            }
        }
        present(vc, animated: true)
            
    }
}

